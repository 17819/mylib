/* 
Installation script for Development environments
Version: 1-1-0

This script is used in order to initialize development environment for MyLib application.

Warning!!!
This script removes old MyLib DB and creates if from zero.
This enables to refresh environment, but you have to be careful to not lose your current work.

*/

-- 1.0 CREATE DATABASE
DROP DATABASE IF EXISTS MyLib;
CREATE DATABASE IF NOT EXISTS MyLib;
ALTER DATABASE MyLib DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE MyLib;

-- 2.0 CREATE TABLES
CREATE TABLE `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(75) NOT NULL,
  `SURNAME` varchar(75) NOT NULL,
  `PESEL` varchar(11) NOT NULL,
  `SOCIAL_STATUS` varchar(2) NOT NULL,
  `BIRTH_DATE` date NOT NULL,
  `CITY` varchar(45) NOT NULL,
  `STREET` varchar(75) NOT NULL,
  `HOUSE_NUMBER` varchar(4) NOT NULL,
  `APARTMENT_NUMBER` varchar(4) DEFAULT NULL,
  `POSTAL_CODE` varchar (5) NOT NULL,
  `USERNAME` varchar(45) NOT NULL,
  `PASSWORD` varchar(25) NOT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `ENABLED` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`USERNAME`),
  UNIQUE KEY `PESEL_UNIQUE` (`PESEL`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  UNIQUE KEY `USERNAME_UNIQUE` (`USERNAME`)
);

CREATE TABLE user_roles (
  `USER_ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(45) NOT NULL,
  `ROLE` varchar(45) NOT NULL,
  PRIMARY KEY (USER_ROLE_ID),
  UNIQUE KEY uni_username_role (role, username),
  KEY fk_username_idx (username),
  CONSTRAINT fk_username FOREIGN KEY (USERNAME) REFERENCES user (USERNAME)
  ON DELETE CASCADE ON UPDATE CASCADE);

CREATE TABLE `library` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `STREET` VARCHAR(45) NOT NULL,
  `HOUSE_NUMBER` VARCHAR(4) NOT NULL,
  `POSTAL_CODE` VARCHAR(5) NOT NULL,
  `TELEPHONE` VARCHAR(9) NOT NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `working_hours` (
    `ID` INT NOT NULL AUTO_INCREMENT,
    `START_TIME` VARCHAR(5) NOT NULL,
    `END_TIME` VARCHAR(5) NOT NULL,
    `DAY_OF_WEEK` VARCHAR(20) NOT NULL,
     PRIMARY KEY (`ID`)
);

CREATE TABLE `book` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUTHOR_NAME` varchar(75) NOT NULL,
  `AUTHOR_SURNAME` varchar(75) NOT NULL,
  `TITLE` varchar(150) NOT NULL,
  `PUBLISHER` varchar(100) NOT NULL,
  `ISBN` varchar(17) NOT NULL,
  `SIGNATURE` varchar(6) NOT NULL,
  `DESCRIPTION` varchar(1500) NOT NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `reservation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_BOOK` int(11) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `ID_LIBRARY` int(2) NOT NULL,
  `RENTAL_DATE` date NOT NULL,
  `RETURN_DATE` date NOT NULL,
  `STATUS` varchar(2),
  PRIMARY KEY (`ID`),
  KEY `ID_BOOK_FK_idx` (`ID_BOOK`),
  KEY `ID_USER_FK_idx` (`ID_USER`),
  CONSTRAINT `ID_BOOK_FK` FOREIGN KEY (`ID_BOOK`) REFERENCES `book` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ID_USER_FK` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE `book_copies` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `ID_BOOK` int(11) NOT NULL,
   `ID_LIBRARY` int(11) NOT NULL,
   `FREE` int(11) NOT NULL,
   `MAX` int(11) NOT NULL,
   PRIMARY KEY (`ID`)
);

-- 3.0 TEST DATA
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (1,  'Bolesław', 'Prus', 'Lalka', 'Wydawnictwo MG', '9788377792063', 'LS0001', 'Lalka Bolesława Prusa to utwór wyjątkowy, od momentu ukazania się na łamach „Kuriera Codziennego” budzący kontrowersje, ale też podziw. Zawikłana, pełna niedopowiedzeń historia źle ulokowanych uczuć, straconych złudzeń i zaprzepaszczonych możliwości. Jedyna w swoim rodzaju konfrontacja romantycznego i pozytywistycznego idealizmu z realizmem.');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (2,  'Bolesław', 'Prus', 'Katarynka', 'Greg', '9788375178371', 'LS0002', 'Życie potrafi nas zaskoczyć, szczególnie wtedy, kiedy najmniej się tego spodziewamy. Pan Tomasz nie domyślał się, że kiedykolwiek będzie w stanie słuchać dźwięku katarynki. Nie lubił hałasu, a jego świat był niezwykle uporządkowany. Jedna sytuacja, chwila, przemyślenie, sprawiło, że całkowicie zmienił spojrzenie na wszystko. “Katarynka” to nowela, która mówi o potrzebie pomagania najuboższym i każdemu, kto tej pomocy od nas potrzebuje.');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (3,  'Adam', 'Mickiewicz', 'Ballady i romanse', 'Greg', '9788375171860', 'LS0003', 'Ballady i romanse Adama Mickiewicza to zbiór utworów bardzo ważnych dla polskiej literatury - otwierają one epokę polskiego Romantyzmu. Każda z ballad niesie ze sobą językową świeżość i oryginalność, a także charakterystyczne dla epoki treści i przesłania. Mamy tu więc nieszczęśliwą miłość, morderstwo, duchy, nadnaturalne interwencje i błąkające się upiory, groźną naturę wymierzającą kary złym ludziom i surowe, ale zawsze sprawiedliwe boskie wyroki.');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (4,  'George', 'Orwell', 'Folwark zwierzęcy', 'Muza', '9789863182085', 'LS0004', 'N/A');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (5,  'Adam', 'Mickiewicz', 'Dziady', 'Greg', '9789863184225', 'LS0005', 'N/A');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (6,  'Michaił', 'Bułhakow', 'Mistrz i Małgorzata', 'Bellona', '9780436350306', 'LS0006', 'N/A');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (7,  'Fiodor', 'Dostojewski', 'Zbrodnia i kara', 'W.A.B.', '9781595404299', 'LS0007', 'N/A');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (8,  'Henryk', 'Sienkiewicz', 'Quo Vadis', 'Greg', '9788182500006', 'LS0008', 'N/A');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (9,  'George', 'Orwell', 'Córka proboszcza', 'Muza', '9789569197284', 'LS0009', 'N/A');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (10, 'Ernest', 'Hemingway', 'Stary człowiek i morze', 'Muza', '9789588962474', 'LS0010', 'N/A');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (11, 'Tadeusz', 'Borowski', 'Pożegnanie z Marią', 'Książka i Wiedza', '9783506430182', 'LS0011', 'N/A');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (12, 'Jan', 'Kochanowski', 'Fraszki', 'Ossolineum', '9781481540544', 'LS0012', 'N/A');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (13, 'Czesław', 'Miłowsz', 'Wiersze', 'Ludowa Spółdzielnia Wydawnicza', '9780436350306', 'LS0013', 'N/A');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (14, 'Stanisław', 'Lem', 'Opowieści o pilocie Pirxie', 'Wydawnictwo Literackie', '9789863184225', 'LS0014', 'N/A');
INSERT INTO book (ID, AUTHOR_NAME, AUTHOR_SURNAME, TITLE, PUBLISHER, ISBN, SIGNATURE, DESCRIPTION) VALUES (15, 'Bruno', 'Schulz', 'Sklepy cynamonowe', 'Greg', '9780436350306', 'LS0015', 'N/A');

INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (1, 1, 1, 3, 3);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (2, 1, 2, 0, 4);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (3, 2, 1, 2, 5);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (4, 2, 2, 1, 3);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (5, 3, 1, 0, 2);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (6, 3, 2, 3, 6);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (7, 4, 1, 4, 7);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (8, 4, 2, 2, 7);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (9, 5, 1, 6, 15);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (10, 5, 2, 4, 12);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (11, 6, 1, 4, 5);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (12, 6, 2, 0, 5);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (13, 7, 1, 7, 10);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (14, 7, 2, 11, 12);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (15, 8, 1, 3, 9);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (16, 8, 2, 5, 8);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (17, 9, 1, 2, 5);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (18, 9, 2, 4, 7);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (19, 10, 1, 0, 5);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (20, 10, 2, 2, 3);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (21, 11, 1, 2, 7);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (22, 11, 2, 1, 4);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (23, 12, 1, 7, 12);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (24, 12, 2, 1, 9);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (25, 13, 1, 0, 3);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (26, 13, 2, 2, 5);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (27, 14, 1, 2, 7);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (28, 14, 2, 1, 4);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (29, 15, 1, 7, 7);
INSERT INTO book_copies (ID, ID_BOOK, ID_LIBRARY, FREE, MAX) VALUES (30, 15, 2, 11, 13);

INSERT INTO user(ID, NAME, SURNAME, PESEL, SOCIAL_STATUS, BIRTH_DATE, CITY, STREET, HOUSE_NUMBER, POSTAL_CODE, USERNAME, PASSWORD, EMAIL, ENABLED)
VALUES (1, 'Andrzej', 'Kwaśniewski', '12345678901', 'EP', '1990-12-31', 'Warszawa', 'Chłodnicza', '19', '12345', 'admin', 'admin1', 'aaa@wp.pl', 1);
INSERT INTO user(ID, NAME, SURNAME, PESEL, SOCIAL_STATUS, BIRTH_DATE, CITY, STREET, HOUSE_NUMBER, POSTAL_CODE, USERNAME, PASSWORD, EMAIL, ENABLED)
VALUES (2, 'Paweł', 'Wiśniewski', '63462346905', 'EP', '1993-08-29', 'Gdańsk', 'Górska', '15', '35334', 'user', 'user1', 'bbb@wp.pl', 1);
INSERT INTO user(ID, NAME, SURNAME, PESEL, SOCIAL_STATUS, BIRTH_DATE, CITY, STREET, HOUSE_NUMBER, POSTAL_CODE, USERNAME, PASSWORD, EMAIL, ENABLED)
VALUES (3, 'Anna', 'Pracownicza', '98237129602', 'EP', '1984-01-15', 'Gdynia', 'Hutnicza', '25', '44334', 'emp', 'emp1', 'ccc@wp.pl', 1);

INSERT INTO reservation (ID, ID_BOOK, ID_USER, ID_LIBRARY, RENTAL_DATE, RETURN_DATE) VALUES (1, 1, 1, 1, '2019-01-10', '2019-02-10');
INSERT INTO reservation (ID, ID_BOOK, ID_USER, ID_LIBRARY, RENTAL_DATE, RETURN_DATE) VALUES (2, 2, 1, 1, '2019-01-10', '2019-02-10');

INSERT INTO user_roles(USER_ROLE_ID, USERNAME, ROLE) VALUES (1, 'admin', 'ROLE_ADMIN');
INSERT INTO user_roles(USER_ROLE_ID, USERNAME, ROLE) VALUES (2, 'admin', 'ROLE_EMPLOYEE');
INSERT INTO user_roles(USER_ROLE_ID, USERNAME, ROLE) VALUES (3, 'admin', 'ROLE_USER');

INSERT INTO user_roles(USER_ROLE_ID, USERNAME, ROLE) VALUES (4, 'user', 'ROLE_USER');

INSERT INTO user_roles(USER_ROLE_ID, USERNAME, ROLE) VALUES (5, 'emp', 'ROLE_USER');
INSERT INTO user_roles(USER_ROLE_ID, USERNAME, ROLE) VALUES (6, 'emp', 'ROLE_EMPLOYEE');

INSERT INTO library (ID, STREET, HOUSE_NUMBER, POSTAL_CODE, TELEPHONE) VALUES (1, 'Biblioteczna', '10', '99123', '333444555');
INSERT INTO library (ID, STREET, HOUSE_NUMBER, POSTAL_CODE, TELEPHONE) VALUES (2, 'Szkolna', '25', '99123', '333444666');

INSERT INTO working_hours VALUES (1, '8:00', '16:00', 'Poniedziałek');
INSERT INTO working_hours VALUES (2, '9:00', '17:00', 'Wtorek');
INSERT INTO working_hours VALUES (3, '10:00', '18:00', 'Środa');
INSERT INTO working_hours VALUES (4, '9:00', '17:00', 'Czwartek');
INSERT INTO working_hours VALUES (5, '8:00', '16:00', 'Piątek');
INSERT INTO working_hours VALUES (6, '10:00', '16:00', 'Sobota');
INSERT INTO working_hours VALUES (7, '10:00', '14:00', 'Niedziela');