<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/header.jsp" %>
<div class="page-header">
    <h3 class="text-center">Placówki biblioteki</h3>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <table id="libTable" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Placówka</th>
                    <th>Ulica</th>
                    <th>Kod pocztowy</th>
                    <th>Numer telefonu</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="libVar" items="${libraries}">
                    <tr>
                        <td>Biblioteka nr ${libVar.id}</td>
                        <td>${libVar.street}, ${libVar.houseNumber}</td>
                        <td>${libVar.postalCode}</td>
                        <td>${libVar.telephone}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <div>
        <h3 class="text-center">Godziny otwarcia</h3>
    </div>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <table id="openHoursTable" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Dzień</th>
                        <th>Godzina otwarcia</th>
                        <th>Godzina zamknięcia</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var="whVar" items="${workingHours}">
                    <tr>
                        <td>${whVar.dayOfWeek}</td>
                        <td>${whVar.startTime}</td>
                        <td>${whVar.endTime}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-lg-2"></div>
    </div>
</div>
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>
