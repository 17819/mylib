<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page import="com.pjatk.mylib.constants.RequestParams" %>
<%@ page import="com.pjatk.mylib.constants.Const" %>
<%@ page import="java.util.Objects" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/pages/common/header.jsp" %>
<%
    String name = Objects.toString(request.getParameter(RequestParams.AUTHOR_NAME), "");
    String surname = Objects.toString(request.getParameter(RequestParams.AUTHOR_SURNAME), "");
    String title = Objects.toString(request.getParameter(RequestParams.TITLE), "");
    String signature = Objects.toString(request.getParameter(RequestParams.SIGNATURE), "");
    String isbn = Objects.toString(request.getParameter(RequestParams.ISBN), "");
    String publisher = Objects.toString(request.getParameter(RequestParams.PUBLISHER), "");
%>
<div class="page-header">
    <h3 class="text-center">Wyszukiwanie książek</h3>
</div>
<div>
    <spring:url value="/books/search" var="booksUrl"/>
    <form action="${booksUrl}">
        <div class="container">
            <div id="authorBlock" class="row" style="display:block">
                <div class="col-lg-2"><label>Autor - Imię:</label></div>
                <div class="col-lg-4">
                    <input type="text" class="form-control" name="<%=RequestParams.AUTHOR_NAME%>" value="<%=name%>"
                           maxlength="75">
                </div>
                <div class="col-lg-6"></div>
                <div class="col-lg-2"><label>Autor - Nazwisko:</label></div>
                <div class="col-lg-4">
                    <input type="text" class="form-control" name="<%=RequestParams.AUTHOR_SURNAME%>"
                           value="<%=surname%>" maxlength="75">
                </div>
                <div class="col-lg-6"></div>
            </div>
            <div id="titleBlock" class="row" style="display:none">
                <div class="col-lg-2"><label>Tytuł:</label></div>
                <div class="col-lg-4">
                    <input type="text" class="form-control" name="<%=RequestParams.TITLE%>" value="<%=title%>"
                           maxlength="150">
                </div>
                <div class="col-lg-6"></div>
            </div>
            <div id="signatureBlock" class="row" style="display:none">
                <div class="col-lg-2"><label>Sygnatura:</label></div>
                <div class="col-lg-4">
                    <input type="text" class="form-control" name="<%=RequestParams.SIGNATURE%>" value="<%=signature%>"
                           maxlength="6">
                </div>
                <div class="col-lg-6"></div>
            </div>
            <div id="isbnBlock" class="row" style="display:none">
                <div class="col-lg-2"><label>ISBN:</label></div>
                <div class="col-lg-4">
                    <input type="text" class="form-control" name="<%=RequestParams.ISBN%>" value="<%=isbn%>"
                           maxlength="13">
                </div>
                <div class="col-lg-6"></div>
            </div>
            <div id="publisherBlock" class="row" style="display:none">
                <div class="col-lg-2"><label>Wydawca:</label></div>
                <div class="col-lg-4">
                    <input type="text" class="form-control" name="<%=RequestParams.PUBLISHER%>" value="<%=publisher%>"
                           maxlength="100">
                </div>
                <div class="col-lg-6"></div>
            </div>
            <br>
            Szukaj na podstawie:<br>
            <label class="radio-inline">
                <input type="radio" id="radioAuthor" onclick="refreshInputFields(this.id)"
                       name="<%=RequestParams.RADIO_SEARCH_TYPE%>"
                       value="<%=Const.RADIO_VALUE_BY_AUTHOR%>" checked>Autora
            </label>
            <label class="radio-inline">
                <input type="radio" id="radioTitle" onclick="refreshInputFields(this.id)"
                       name="<%=RequestParams.RADIO_SEARCH_TYPE%>"
                       value="<%=Const.RADIO_VALUE_BY_TITLE%>">Tytułu
            </label>
            <label class="radio-inline">
                <input type="radio" id="radioSignature" onclick="refreshInputFields(this.id)"
                       name="<%=RequestParams.RADIO_SEARCH_TYPE%>"
                       value="<%=Const.RADIO_VALUE_BY_SIGNATURE%>">Sygnatury
            </label>
            <label class="radio-inline">
                <input type="radio" id="radioISBN" onclick="refreshInputFields(this.id)"
                       name="<%=RequestParams.RADIO_SEARCH_TYPE%>"
                       value="<%=Const.RADIO_VALUE_BY_ISBN%>">ISBN
            </label>
            <label class="radio-inline">
                <input type="radio" id="radioPublisher" onclick="refreshInputFields(this.id)"
                       name="<%=RequestParams.RADIO_SEARCH_TYPE%>"
                       value="<%=Const.RADIO_VALUE_BY_PUBLISHER%>">Wydawcy
            </label><br><br>
            <button type="submit" class="btn btn-primary">
                <span class="glyphicon glyphicon-search"></span> Szukaj
            </button>
        </div>
    </form>
</div>
<div>
    <table id="bookTable" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Autor</th>
            <th>Tytuł</th>
            <th>Sygnatura</th>
            <th>ISBN</th>
            <th>Wydawca</th>
            <th>Więcej informacji</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="book" items="${books}">
            <tr>
                <td>${book.authorSurname}, ${book.authorName}</td>
                <td>${book.title}</td>
                <td>${book.signature}</td>
                <td>${book.isbn}</td>
                <td>${book.publisher}</td>
                <spring:url value="/books/${book.id}/show" var="bookDetailsURL"/>
                <td><a href="${bookDetailsURL}">Szczegóły</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<script>
    function refreshInputFields(clicked_id) {
        document.getElementById("authorBlock").style.display = "none";
        document.getElementById("titleBlock").style.display = "none";
        document.getElementById("signatureBlock").style.display = "none";
        document.getElementById("isbnBlock").style.display = "none";
        document.getElementById("publisherBlock").style.display = "none";

        if (clicked_id === "radioAuthor") {
            document.getElementById("authorBlock").style.display = "block";
        } else if (clicked_id === "radioTitle") {
            document.getElementById("titleBlock").style.display = "block";
        } else if (clicked_id === "radioSignature") {
            document.getElementById("signatureBlock").style.display = "block";
        } else if (clicked_id === "radioISBN") {
            document.getElementById("isbnBlock").style.display = "block";
        } else if (clicked_id === "radioPublisher") {
            document.getElementById("publisherBlock").style.display = "block";
        }
    }

    refreshInputFields("${clickedRadioButton}");
    document.getElementById("${clickedRadioButton}").checked = true;

    $(document).ready(function () {
        $('#bookTable').DataTable({
            searching: false, paging: false, info: false,
            language: {
                "emptyTable": "Brak wyników",
            },
            columnDefs: [
                {"orderable": false, "targets": 5}
            ]
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>
