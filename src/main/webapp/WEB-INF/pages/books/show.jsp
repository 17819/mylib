<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.pjatk.mylib.constants.Const" %>
<%@ include file="/WEB-INF/pages/common/header.jsp" %>
<%--@elvariable id="<%=Const.MODEL_BOOK%>" type=""--%>
<sec:authorize access="!hasRole('USER')">
    <jsp:include page="../common/info.jsp"/>
</sec:authorize>
<c:if test="${not empty msgError}">
    <jsp:include page="../common/error.jsp"/>
</c:if>
<c:if test="${not empty msgSuccess}">
    <jsp:include page="../common/success.jsp"/>
</c:if>
<form:form class="form-horizontal" modelAttribute="<%=Const.MODEL_BOOK%>">
    <div class="container">
        <div class="row">
            <div class="col-lg-2"><label>Autor - Imię</label></div>
            <div class="col-lg-6"><form:input path="authorName" type="text" class="form-control" readonly="true"/></div>
            <div class="col-lg-4"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label>Autor - Nazwisko</label></div>
            <div class="col-lg-6"><form:input path="authorSurname" type="text" class="form-control"
                                              readonly="true"/></div>
            <div class="col-lg-4"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label>Tytuł</label></div>
            <div class="col-lg-6"><form:input path="title" type="text" class="form-control" readonly="true"/></div>
            <div class="col-lg-4"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label>Wydawca</label></div>
            <div class="col-lg-6"><form:input path="publisher" type="text" class="form-control" readonly="true"/></div>
            <div class="col-lg-4"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label>ISBN</label></div>
            <div class="col-lg-6"><form:input path="isbn" type="text" class="form-control" readonly="true"/></div>
            <div class="col-lg-4"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label>Sygnatura</label></div>
            <div class="col-lg-6"><form:input path="signature" type="text" class="form-control" readonly="true"/></div>
            <div class="col-lg-4"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label>Opis</label></div>
            <div class="col-lg-8"><form:textarea path="description" type="text" class="form-control" readonly="true"
                                                 rows="8"
                                                 cssStyle="resize: none"/></div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <table id="openHoursTable" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Placówka biblioteczna</th>
                        <th>Ilość dostępnych egzemplarzy</th>
                        <th>Ilość wszystkich egzemplarzy</th>
                        <th>Zamów</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="copyVar" items="${copies}">
                        <tr>
                            <td>Biblioteka nr ${copyVar.libraryId}</td>
                            <td>${copyVar.free}</td>
                            <td>${copyVar.max}</td>
                            <td>
                                <sec:authorize access="hasRole('USER')">
                                    <c:if test="${copyVar.free ge 1}">
                                        <spring:url value="/books/order/${book.id}/${copyVar.libraryId}" var="orderUrl"/>
                                        <a href="${orderUrl}">
                                            <button type="button" class="btn btn-primary center-block">
                                                <span class="glyphicon glyphicon-book"></span> Zamów
                                            </button>
                                        </a>
                                    </c:if>
                                </sec:authorize>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row">
            <div class="col-lg-5"></div>
            <div class="col-lg-2">
                <br><br>
                <button type="button" class="btn btn-primary center-block" onclick="history.go(-1);">
                    <span class="glyphicon glyphicon-circle-arrow-left"></span> Powrót
                </button>
            </div>
            <div class="col-lg-5"></div>
        </div>
    </div>
</form:form>
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>
