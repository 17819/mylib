<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/pages/common/header.jsp" %>
<div class="page-header">
    <h3 class="text-center">Logowanie</h3>
</div>
<div>
    <c:if test="${not empty msgError}">
        <jsp:include page="../common/error.jsp"/>
    </c:if>
    <c:if test="${not empty msgSuccess}">
        <jsp:include page="../common/success.jsp"/>
    </c:if>
    <div id="login-box">
        <spring:url value="/login" var="loginUrl"/>
        <form name='loginForm' action="${loginUrl}" method='POST'>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login" type="text" class="form-control" name="username"
                               placeholder="Nazwa użytkownika" maxlength="25">
                    </div>
                    <div class="col-lg-4"></div>
                </div>
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="password" type="password" class="form-control" name="password" placeholder="Hasło"
                               maxlength="25">
                    </div>
                    <div class="col-lg-4"></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <button type="submit" class="btn btn-success center-block">
                            <span class="glyphicon glyphicon-ok"></span> Zaloguj
                        </button>
                    </div>
                    <div class="col-lg-4"></div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </div>
        </form>
    </div>
</div>
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>