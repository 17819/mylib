<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/header.jsp" %>
<div class="page-header">
    <h3 class="text-center">Menu zarządzania czytelnikami</h3>
</div>
<c:if test="${not empty msgSuccess}">
    <jsp:include page="../common/success.jsp"/>
</c:if>
<c:if test="${not empty msgInfo}">
    <jsp:include page="../common/info.jsp"/>
</c:if>
<c:if test="${not empty msgError}">
    <jsp:include page="../common/error.jsp"/>
</c:if>
<div class="container">
    <div class="row">
        <div class="col-lg-4"></div>
        <spring:url value="/user/management/create" var="createUrl"/>
        <div class="col-lg-2">
            <a href="${createUrl}">
                <button type="button" class="btn btn-success center-block">
                    <span class="glyphicon glyphicon-user"></span> Utwórz konto
                </button>
            </a>
        </div>
        <spring:url value="/user/management/search" var="searchUrl"/>
        <div class="col-lg-2">
            <a href="${searchUrl}">
                <button type="button" class="btn btn-primary center-block">
                    <span class="glyphicon glyphicon-search"></span> Szukaj czytelnika
                </button>
            </a>
        </div>
        <div class="col-lg-4"></div>
    </div>
</div>
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>