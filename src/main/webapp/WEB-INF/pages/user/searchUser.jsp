<%@ page import="com.pjatk.mylib.constants.RequestParams" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="com.pjatk.mylib.constants.Const" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/header.jsp" %>
<div class="page-header">
    <h3 class="text-center">Szukaj czytelnika</h3>
</div>
<c:if test="${not empty msgSuccess}">
    <jsp:include page="../common/success.jsp"/>
</c:if>
<c:if test="${not empty msgInfo}">
    <jsp:include page="../common/info.jsp"/>
</c:if>
<c:if test="${not empty msgError}">
    <jsp:include page="../common/error.jsp"/>
</c:if>
<spring:url value="/user/management/search" var="searchUserUrl"/>
<div id="searchBox">
    <form name='insertUserForm' action="${searchUserUrl}" method='POST'>
        <div class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <p class="text-center">Szukaj na podstawie</p>
                    <input id="<%=RequestParams.USERNAME%>" type="text" class="form-control"
                           name="<%=RequestParams.USERNAME%>" placeholder="Nazwa użytkownika" maxlength="75">
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <p class="text-center">lub</p>
                    <input id="<%=RequestParams.PESEL%>" type="text" class="form-control"
                           name="<%=RequestParams.PESEL%>" placeholder="PESEL" maxlength="75">
                </div>
                <div class="col-lg-3"></div>
            </div><br>
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4">
                    <button type="submit" class="btn btn-primary center-block">
                        <span class="glyphicon glyphicon-search"></span> Szukaj
                    </button>
                </div>
                <div class="col-lg-4"></div>
            </div>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
    <c:if test="${not empty user}">
        <br>
        <div class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"><label>Imię</label></div>
                <div class="col-lg-3">${user.name}</div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"><label>Nazwisko</label></div>
                <div class="col-lg-3">${user.surname}</div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"><label>PESEL</label></div>
                <div class="col-lg-3">${user.pesel}</div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"><label>Data urodzenia</label></div>
                <div class="col-lg-3"><p id="birthDateText">${user.birthDate}</p></div>
                <div class="col-lg-3">
                    <script>
                        var element = document.getElementById("birthDateText");
                        var value = element.innerHTML.substring(0, 10);
                        element.innerHTML = value;
                    </script>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"><label>Status społeczny</label></div>
                <div class="col-lg-3"><p id="socialStatusText">${user.socialStatus}</p></div>
                <div class="col-lg-3">
                    <script>
                        var element = document.getElementById("socialStatusText");
                        var value = element.innerHTML;
                        if (value == "<%=Const.SOCIAL_STATUS_EMPLOYED_PERSON%>") {
                            value = "Zatrudniony";
                        } else if (value == "<%=Const.SOCIAL_STATUS_UNEMPLOYED%>") {
                            value = "Bezrobotny";
                        } else if (value == "<%=Const.SOCIAL_STATUS_STUDENT%>") {
                            value = "Student";
                        } else if (value == "<%=Const.SOCIAL_STATUS_PUPIL%>") {
                            value = "Uczeń";
                        }
                        element.innerHTML = value;
                    </script>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"><label>Nazwa użytkownika</label></div>
                <div class="col-lg-3">${user.username}</div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"><label>Miasto</label></div>
                <div class="col-lg-3">${user.city}</div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"><label>Ulica</label></div>
                <div class="col-lg-3">${user.street}</div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"><label>Numer domu</label></div>
                <div class="col-lg-3">${user.houseNumber}</div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"><label>Numer mieszkania</label></div>
                <div class="col-lg-3">${user.apartmentNumber}</div>
                <div class="col-lg-3"></div>
            </div>
        </div><br>
        <div class="container">
            <div class="row">
                <div class="col-lg-4"></div>
                <spring:url value="/user/management/modify/${user.username}" var="modifyUrl"/>
                <div class="col-lg-2">
                    <a href="${modifyUrl}">
                        <button type="button" class="btn btn-primary center-block">
                            <span class="glyphicon glyphicon-pencil"></span> Modyfikuj konto
                        </button>
                    </a>
                </div>
                <spring:url value="/user/management/delete/${user.username}" var="deleteUrl"/>
                <div class="col-lg-2">
                    <a href="${deleteUrl}" onclick="return confirm('Czy na pewno chcesz usunąć tego użytkownika?');">
                        <button type="button" class="btn btn-danger center-block">
                            <span class="glyphicon glyphicon-remove"></span> Usuń konto
                        </button>
                    </a>
                </div>
                <div class="col-lg-4"></div>
            </div>
        </div>
    </c:if>
</div>
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>