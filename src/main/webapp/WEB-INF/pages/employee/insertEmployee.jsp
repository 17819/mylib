<%@ page import="com.pjatk.mylib.constants.Const" %>
<%@ page import="com.pjatk.mylib.constants.RequestParams" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/header.jsp" %>
<div class="page-header">
    <h3 class="text-center">Utwórz konto pracownika</h3>
</div>
<c:if test="${not empty msgSuccess}">
    <jsp:include page="../common/success.jsp"/>
</c:if>
<c:if test="${not empty msgInfo}">
    <jsp:include page="../common/info.jsp"/>
</c:if>
<c:if test="${not empty msgError}">
    <jsp:include page="../common/error.jsp"/>
</c:if>
<spring:url value="/employee/management/create" var="insertEmployeeUrl"/>
<%
    String today;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    today = sdf.format(new Date());
%>
<div id="insert-box">
    <%--@elvariable id="<%=Const.MODEL_USER%>" type=""--%>
    <form:form name='insertUserForm' action="${insertEmployeeUrl}" method='POST' modelAttribute="<%=Const.MODEL_USER%>">
        <div class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <form:input id="<%=RequestParams.NAME%>" type="text" class="form-control"
                                path="<%=RequestParams.NAME%>" placeholder="Imię" maxlength="75"/>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <form:input id="<%=RequestParams.SURNAME%>" type="text" class="form-control"
                                path="<%=RequestParams.SURNAME%>" placeholder="Nazwisko" maxlength="75"/>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <form:input id="<%=RequestParams.PESEL%>" type="text" class="form-control"
                                path="<%=RequestParams.PESEL%>" placeholder="PESEL" maxlength="11"/>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <form:input id="<%=RequestParams.EMAIL%>" type="text" class="form-control"
                                path="<%=RequestParams.EMAIL%>" placeholder="Email" maxlength="50"/>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <label for="<%=RequestParams.SOCIAL_STATUS%>">Status społeczny</label>
                    <form:select class="form-control" id="<%=RequestParams.SOCIAL_STATUS%>" path="<%=RequestParams.SOCIAL_STATUS%>">
                        <option value="<%=Const.SOCIAL_STATUS_PUPIL%>">Uczeń</option>
                        <option value="<%=Const.SOCIAL_STATUS_STUDENT%>">Student</option>
                        <option value="<%=Const.SOCIAL_STATUS_EMPLOYED_PERSON%>">Zatrudniony</option>
                        <option value="<%=Const.SOCIAL_STATUS_UNEMPLOYED%>">Bezrobotny</option>
                    </form:select>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <label for="<%=RequestParams.BIRTH_DATE%>">Data urodzenia</label>
                    <form:input id="<%=RequestParams.BIRTH_DATE%>" type="date" class="form-control"
                                path="<%=RequestParams.BIRTH_DATE%>" min="1900-01-01" max="<%=today%>" value="<%=today%>"/>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <form:input id="<%=RequestParams.CITY%>" type="text" class="form-control"
                                path="<%=RequestParams.CITY%>" placeholder="Miasto" maxlength="45"/>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <form:input id="<%=RequestParams.STREET%>" type="text" class="form-control"
                                path="<%=RequestParams.STREET%>" placeholder="Ulica" maxlength="75"/>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <form:input id="<%=RequestParams.HOUSE_NUMBER%>" type="text" class="form-control"
                                path="<%=RequestParams.HOUSE_NUMBER%>" placeholder="Numer domu" maxlength="4"/>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <form:input id="<%=RequestParams.APARTMENT_NUMBER%>" type="text" class="form-control"
                                path="<%=RequestParams.APARTMENT_NUMBER%>" placeholder="Numer mieszkania" maxlength="4"/>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <form:input id="<%=RequestParams.POSTAL_CODE%>" type="text" class="form-control"
                                path="<%=RequestParams.POSTAL_CODE%>" placeholder="Kod pocztowy" maxlength="5"/>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4">
                    <button type="submit" class="btn btn-success center-block">
                        <span class="glyphicon glyphicon-ok"></span> Utwórz Konto
                    </button>
                </div>
                <div class="col-lg-4"></div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>
    </form:form>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-5"></div>
        <spring:url value="/employee/management/menu" var="menuUrl"/>
        <div class="col-lg-2">
            <a href="${menuUrl}">
                <button type="button" class="btn btn-primary center-block">
                    <span class="glyphicon glyphicon-circle-arrow-left"></span> Powrót
                </button>
            </a>
        </div>
        <div class="col-lg-5"></div>
    </div>
</div>
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>