<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<nav role="navigation" class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <spring:url value="/" var="homeUrl"/>
            <a class="navbar-brand" href="${homeUrl}">Strona główna</a>
        </div>
        <ul class="nav navbar-nav">
            <spring:url value="/books/search?radioType=byAuthor" var="booksUrl"/>
            <li><a href="${booksUrl}">Książki</a></li>

            <spring:url value="/account/info" var="accountUrl"/>
            <li><a href="${accountUrl}">Moje Konto</a></li>

            <sec:authorize access="hasRole('USER')">
                <spring:url value="/account/reservations" var="reservationsUrl"/>
                <li><a href="${reservationsUrl}">Rezerwacje</a></li>
            </sec:authorize>

            <spring:url value="/library/info" var="libraryInfoUrl"/>
            <li><a href="${libraryInfoUrl}">Nasze Placówki</a></li>

            <sec:authorize access="hasRole('EMPLOYEE')">
                <spring:url value="/user/management/menu" var="userManagementUrl"/>
                <li><a href="${userManagementUrl}">Czytelnicy</a></li>
            </sec:authorize>

            <sec:authorize access="hasRole('ADMIN')">
                <spring:url value="/employee/management/menu" var="userManagementUrl"/>
                <li><a href="${userManagementUrl}">Pracownicy</a></li>
            </sec:authorize>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <sec:authorize access="!hasRole('USER')">
                <spring:url value="/login" var="loginUrl"/>
                <li><a href="${loginUrl}"><span class="glyphicon glyphicon-user"></span> Zaloguj</a></li>
            </sec:authorize>
            <sec:authorize access="hasRole('USER')">
                <spring:url value="/logout" var="logoutUrl"/>
                <li><a href="${logoutUrl}"><span class="glyphicon glyphicon-log-in"></span> Wyloguj</a></li>
            </sec:authorize>
        </ul>
    </div>
</nav>
