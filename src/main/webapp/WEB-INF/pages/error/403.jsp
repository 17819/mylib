<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/header.jsp"%>
<h1>HTTP Status 403 - Odmowa dostępu</h1>
<h2>Dostęp do tej strony wymaga zalogowania na konto, które posiada odpowiednie uprawnienia!</h2>
<%@ include file="/WEB-INF/pages/common/footer.jsp"%>
