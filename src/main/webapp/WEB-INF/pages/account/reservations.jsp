<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/header.jsp" %>
<div class="page-header">
    <h3 class="text-center">Rezerwacje</h3>
</div>
<c:if test="${not empty msgInfo}">
    <jsp:include page="../common/info.jsp"/>
</c:if>
<div class="container">
    <div class="row">
        <table id="libTable" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Książka</th>
                <th>Placówka</th>
                <th>Data wypożyczenia</th>
                <th>Data zwrotu</th>
            </tr>
            </thead>
            <tbody>
            <%
                int counter = 0;
            %>
            <c:forEach var="resevationVar" items="${reservations}">
                <tr>
                    <spring:url value="/books/${resevationVar.idBook}/show" var="bookCheckUrl"/>
                    <td>
                        <a href="${bookCheckUrl}">
                            <button type="button" class="btn btn-primary">
                                <span class="glyphicon glyphicon-search"></span> Szczegóły
                            </button>
                        </a>
                    </td>
                    <td>Biblioteka nr ${resevationVar.idLibrary}</td>
                    <td>
                        <p id="rentalDate<%=counter%>">${resevationVar.rentalDate}</p>
                        <script>
                            var element = document.getElementById("rentalDate<%=counter%>");
                            var value = element.innerHTML.substring(0, 10);
                            element.innerHTML = value;
                        </script>
                    </td>
                    <td>
                        <p id="reservationDate<%=counter%>">${resevationVar.returnDate}</p>
                        <script>
                            var element = document.getElementById("reservationDate<%=counter++%>");
                            var value = element.innerHTML.substring(0, 10);
                            element.innerHTML = value;
                        </script>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</div>
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>
