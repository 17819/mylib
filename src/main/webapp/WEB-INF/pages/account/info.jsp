<%@ page import="com.pjatk.mylib.constants.Const" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/header.jsp" %>
<div class="page-header">
    <h3 class="text-center">Moje konto</h3>
</div>

<%--@elvariable id="<%=Const.MODEL_USER%>" type=""--%>
<form:form class="form-horizontal" modelAttribute="<%=Const.MODEL_USER%>">
    <div class="container">
        <div class="row">
            <div class="col-lg-2"><label>Imię</label></div>
            <div class="col-lg-5"><form:input path="name" type="text" class="form-control" readonly="true"/></div>
            <div class="col-lg-5"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label>Nazwisko</label></div>
            <div class="col-lg-5"><form:input path="surname" type="text" class="form-control" readonly="true"/></div>
            <div class="col-lg-5"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label>PESEL</label></div>
            <div class="col-lg-5"><form:input path="pesel" type="text" class="form-control" readonly="true"/></div>
            <div class="col-lg-5"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label>Status socjalny</label></div>
            <div class="col-lg-5"><form:input path="socialStatus" type="text" class="form-control"
                                              readonly="true"/></div>
            <div class="col-lg-3">
                <a href="#" title="" data-toggle="popover" data-placement="right"
                   data-content="<strong>ST</strong> - Student<br><strong>UC</strong> - Uczeń<br><strong>UN</strong> - Bezrobotny<br><strong>EP</strong> - zatrudniony"
                   data-html="true" data-original-title="Status socjalny">
                    <button type="button" class="btn btn-primary btn-md">
                        <span class="glyphicon glyphicon-question-sign"></span> Info
                    </button>
                </a>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label>Email</label></div>
            <div class="col-lg-5"><form:input path="email" type="text" class="form-control" readonly="true"/></div>
            <div class="col-lg-5"></div>
        </div>
    </div>
</form:form>
<div class="container">
    <div class="row">
        <div class="col-lg-3"></div>
        <spring:url value="/account/changepass" var="changePassUrl"/>
        <div class="col-lg-2">
            <a href="${changePassUrl}">
                <button type="button" class="btn btn-primary center-block">
                    <span class="glyphicon glyphicon-pencil"></span> Zmień hasło
                </button>
            </a>
        </div>
        <spring:url value="/account/changemail" var="changeMailUrl"/>
        <div class="col-lg-2">
            <a href="${changeMailUrl}">
                <button type="button" class="btn btn-primary center-block">
                    <span class="glyphicon glyphicon-pencil"></span> Zmień email
                </button>
            </a>
        </div>
        <div class="col-lg-5"></div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
</script>
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>
