<%@ page import="com.pjatk.mylib.constants.RequestParams" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/header.jsp" %>
<div class="page-header">
    <h3 class="text-center">Zmiana hasła</h3>
</div>
<div>
    <c:if test="${not empty msgError}">
        <jsp:include page="../common/error.jsp"/>
    </c:if>
    <c:if test="${not empty msgSuccess}">
        <jsp:include page="../common/success.jsp"/>
    </c:if>
    <spring:url value="/account/changepass" var="changePassUrl"/>
    <div id="change-box">
        <form name='changePassForm' action="${changePassUrl}" method='POST'>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <input id="<%=RequestParams.OLD_PASSOWRD%>" type="password" class="form-control"
                               name="<%=RequestParams.OLD_PASSOWRD%>" placeholder="Stare hasło" maxlength="25">
                    </div>
                    <div class="col-lg-4"></div>
                </div>
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <input id="<%=RequestParams.PASSWORD%>" type="password" class="form-control"
                               name="<%=RequestParams.PASSWORD%>" placeholder="Nowe hasło" maxlength="25">
                    </div>
                    <div class="col-lg-4"></div>
                </div>
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <input id="<%=RequestParams.PASSWORD_CONFIRMATION%>" type="password" class="form-control"
                               name="<%=RequestParams.PASSWORD_CONFIRMATION%>" placeholder="Powtórz nowe hasło" maxlength="25">
                    </div>
                    <div class="col-lg-4"></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <button type="submit" class="btn btn-success center-block">
                            <span class="glyphicon glyphicon-ok"></span> Zmień hasło
                        </button>
                    </div>
                    <div class="col-lg-4"></div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </div>
        </form>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5"></div>
            <spring:url value="/account/info" var="accountUrl"/>
            <div class="col-lg-2">
                <a href="${accountUrl}">
                    <button type="button" class="btn btn-primary center-block">
                        <span class="glyphicon glyphicon-circle-arrow-left"></span> Powrót
                    </button>
                </a>
            </div>
            <div class="col-lg-5"></div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/pages/common/footer.jsp" %>