package com.pjatk.mylib.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "library")
@NamedQueries({
        @NamedQuery(
                name = Library.FIND_ALL_LIBRARIES,
                query = "SELECT l FROM Library l"
        )
})
public class Library extends BaseEntity {
    public static final String FIND_ALL_LIBRARIES = "Library.findAll";

    @Column(name="street")
    @Size(max=45)
    @NotNull
    private String street;

    @Column(name="house_number")
    @Size(max=4)
    @NotNull
    private String houseNumber;

    @Column(name="postal_code")
    @Size(max=5)
    @NotNull
    private String postalCode;

    @Column(name="telephone")
    @Size(max=9)
    @NotNull
    private String telephone;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
