package com.pjatk.mylib.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="book")
@NamedQueries({
        @NamedQuery(
                name = Book.FIND_BOOKS_BY_AUTHOR_ASC,
                query = "SELECT b FROM Book b WHERE b.authorName = ?1 AND b.authorSurname = ?2 ORDER BY b.authorName, b.authorSurname ASC"
        ),
        @NamedQuery(
                name = Book.FIND_BOOKS_BY_TITLE_ASC,
                query = "SELECT b FROM Book b WHERE b.title = ?1 ORDER BY b.title ASC"
        ),
        @NamedQuery(
                name = Book.FIND_BOOKS_BY_PUBLISHER_ASC,
                query = "SELECT b FROM Book b WHERE b.publisher = ?1 ORDER BY b.publisher ASC"
        ),
        @NamedQuery(
                name = Book.FIND_BOOKS_BY_SIGNATURE_ASC,
                query = "SELECT b FROM Book b WHERE b.signature = ?1 ORDER BY b.signature ASC"
        ),
        @NamedQuery(
                name = Book.FIND_BOOKS_BY_ISBN_ASC,
                query = "SELECT b FROM Book b WHERE b.isbn = ?1 ORDER BY b.isbn ASC"
        ),
})
public class Book extends BaseEntity {
    public static final String FIND_BOOKS_BY_AUTHOR_ASC = "Book.findAllByAuthor";
    public static final String FIND_BOOKS_BY_TITLE_ASC = "Book.findAllByTitle";
    public static final String FIND_BOOKS_BY_PUBLISHER_ASC = "Book.findAllByPublisher";
    public static final String FIND_BOOKS_BY_ISBN_ASC = "Book.findAllByISBN";
    public static final String FIND_BOOKS_BY_SIGNATURE_ASC = "Book.findAllBySignature";

    @NotNull
    @Size(max = 75)
    @Column(name="author_name")
    private String authorName;

    @NotNull
    @Size(max = 75)
    @Column(name="author_surname")
    private String authorSurname;

    @NotNull
    @Size(max = 150)
    @Column(name="title")
    private String title;

    @NotNull
    @Size(max = 100)
    @Column(name="publisher")
    private String publisher;

    @NotNull
    @Size(max = 13)
    @Column(name="isbn")
    private String isbn;

    @NotNull
    @Size(max = 6)
    @Column(name="signature")
    private String signature;

    @NotNull
    @Size(max = 1500)
    @Column(name="description")
    private String description;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorSurname() {
        return authorSurname;
    }

    public void setAuthorSurname(String authorSurname) {
        this.authorSurname = authorSurname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
