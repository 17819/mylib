package com.pjatk.mylib.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "working_hours")
@NamedQueries({
        @NamedQuery(
                name = WorkingHours.FIND_ALL,
                query = "SELECT wk FROM WorkingHours wk"
        )
})
public class WorkingHours extends BaseEntity{
    public static final String FIND_ALL = "WorkingHours.findALl";

    @Column(name = "start_time")
    @Size(max=5)
    @NotNull
    private String startTime;

    @Column(name = "end_time")
    @Size(max=5)
    @NotNull
    private String endTime;

    @Column(name = "day_of_week")
    @Size(max=20)
    @NotNull
    private String dayOfWeek;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
}
