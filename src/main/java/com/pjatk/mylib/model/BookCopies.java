package com.pjatk.mylib.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="book_copies")
@NamedQueries({
        @NamedQuery(
                name = BookCopies.FIND_ALL_FREE_COPIES,
                query = "SELECT b FROM BookCopies b WHERE b.bookId = ?1 ORDER BY b.libraryId ASC"
        )
})
@SqlResultSetMappings({
        @SqlResultSetMapping(
                name="updateBookCount",
                columns = { @ColumnResult(name = "free")}
        )
})
@NamedNativeQueries({
        @NamedNativeQuery(
                name    =   BookCopies.DECREASE_AVAILABLE_COPIES,
                query   =   "UPDATE book_copies SET free = free - 1 WHERE id_book = ?1 AND id_library = ?2",
                resultSetMapping = "updateBookCount"
        )
})
public class BookCopies extends BaseEntity {
    public static final String FIND_ALL_FREE_COPIES = "Book.findAllFreeCopies";
    public static final String DECREASE_AVAILABLE_COPIES = "Book.decreaseAvailableCopies";

    @NotNull
    @Column(name="id_book")
    private Integer bookId;

    @NotNull
    @Column(name="id_library")
    private Integer libraryId;

    @NotNull
    @Column(name="free")
    private Integer free;

    @NotNull
    @Column(name="max")
    private Integer max;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public Integer getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(Integer libraryId) {
        this.libraryId = libraryId;
    }

    public Integer getFree() {
        return free;
    }

    public void setFree(Integer free) {
        this.free = free;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }
}
