package com.pjatk.mylib.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name="reservation")
@NamedQueries({
        @NamedQuery(
                name = Reservation.FIND_ALL_BY_USER_ID,
                query = "SELECT r FROM Reservation r WHERE r.idUser = ?1"
        )
})
public class Reservation extends BaseEntity {
    public static final String FIND_ALL_BY_USER_ID = "Reservation.findAllByUserId";

    @NotNull
    @Column(name="id_book")
    private Integer idBook;

    @NotNull
    @Column(name="id_user")
    private Integer idUser;

    @NotNull
    @Column(name="id_library")
    private Integer idLibrary;

    @NotNull
    @Column(name="rental_date")
    private Date rentalDate;

    @NotNull
    @Column(name="return_date")
    private Date returnDate;

    @Column(name="status")
    private String status;

    public Integer getIdBook() {
        return idBook;
    }

    public void setIdBook(Integer idBook) {
        this.idBook = idBook;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdLibrary() {
        return idLibrary;
    }

    public void setIdLibrary(Integer idLibrary) {
        this.idLibrary = idLibrary;
    }

    public Date getRentalDate() {
        return rentalDate;
    }

    public void setRentalDate(Date rentalDate) {
        this.rentalDate = rentalDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
