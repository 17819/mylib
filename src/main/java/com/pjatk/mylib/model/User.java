package com.pjatk.mylib.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name="user")
@NamedQueries({
        @NamedQuery(
                name = User.FIND_BY_USERNAME,
                query = "SELECT u FROM User u WHERE u.username = ?1"
        ),
        @NamedQuery(
                name = User.FIND_BY_PESEL,
                query = "SELECT u FROM User u WHERE u.pesel = ?1"
        )
})
@SqlResultSetMappings({
        @SqlResultSetMapping(
                name="updatePasswordResult",
                columns = { @ColumnResult(name = "password")}
        ),
        @SqlResultSetMapping(
                name="updateEmailResult",
                columns = { @ColumnResult(name = "email")}
        )
})
@NamedNativeQueries({
        @NamedNativeQuery(
                name    =   User.UPDATE_USER_PASSWORD,
                query   =   "UPDATE user SET password = ?1 WHERE username = ?2",
                resultSetMapping = "updatePasswordResult"
        ),
        @NamedNativeQuery(
                name    =   User.UPDATE_USER_EMAIL,
                query   =   "UPDATE user SET email = ?1 WHERE username = ?2",
                resultSetMapping = "updatePasswordResult"
        ),
        @NamedNativeQuery(
                name    =   User.GET_LAST_ID,
                query   =   "SELECT MAX(id) FROM user"
        ),
        @NamedNativeQuery(
                name    =   User.ASSIGN_USER_ROLE,
                query   =   "INSERT INTO user_roles (USERNAME, ROLE) VALUES (?1, ?2)"
        ),
        @NamedNativeQuery(
                name    =   User.FIND_ALL_USER_ROLES,
                query   =   "SELECT role FROM user_roles WHERE username = ?1"
        ),
        @NamedNativeQuery(
                name    =   User.DELETE_USER,
                query   =   "DELETE FROM user WHERE username = ?1"
        ),
        @NamedNativeQuery(
                name    =   User.UPDATE_USER,
                query   =   "UPDATE user SET NAME = ?1, SURNAME = ?2, PESEL = ?3, EMAIL = ?4, " +
                        "SOCIAL_STATUS = ?5, BIRTH_DATE = ?6, CITY = ?7, STREET = ?8, " +
                        "HOUSE_NUMBER = ?9, APARTMENT_NUMBER = ?10, POSTAL_CODE = ?11 WHERE USERNAME = ?12"
        )
})
public class User extends BaseEntity{
    public static final String FIND_BY_USERNAME = "User.findByUsername";
    public static final String FIND_BY_PESEL = "User.findByPesel";
    public static final String UPDATE_USER_EMAIL = "User.updateEmail";
    public static final String UPDATE_USER_PASSWORD = "User.updatePassword";
    public static final String GET_LAST_ID = "User.getLastID";
    public static final String ASSIGN_USER_ROLE = "User.setUserRole";
    public static final String FIND_ALL_USER_ROLES = "User.findAllRoles";
    public static final String DELETE_USER = "User.delete";
    public static final String UPDATE_USER = "User.updateUser";

    @NotNull
    @Size(max=75)
    @Column(name="name")
    private String name;

    @NotNull
    @Size(max=75)
    @Column(name="surname")
    private String surname;

    @NotNull
    @Size(max=11)
    @Column(name="pesel")
    private String pesel;

    @NotNull
    @Size(max=2)
    @Column(name="social_status")
    private String socialStatus;

    @NotNull
    @Column(name="birth_date")
    private Date birthDate;

    @NotNull
    @Size(max=45)
    @Column(name="city")
    private String city;

    @NotNull
    @Size(max=75)
    @Column(name="street")
    private String street;

    @NotNull
    @Size(max=5)
    @Column(name="postal_code")
    private String postalCode;

    @NotNull
    @Size(max=4)
    @Column(name="house_number")
    private String houseNumber;

    @NotNull
    @Size(max=4)
    @Column(name="apartment_number")
    private String apartmentNumber;

    @Size(max=25)
    @Column(name="username")
    private String username;

    @Size(max=25)
    @Column(name="password")
    private String password;

    @Size(max=50)
    @Column(name="email")
    private String email;

    @Column(nullable = false, name="enabled")
    @Type(type="org.hibernate.type.NumericBooleanType")
    private boolean enabled;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getSocialStatus() {
        return socialStatus;
    }

    public void setSocialStatus(String socialStatus) {
        this.socialStatus = socialStatus;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(String apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
