package com.pjatk.mylib.service;

import com.pjatk.mylib.model.Book;
import com.pjatk.mylib.model.BookCopies;
import com.pjatk.mylib.model.Reservation;
import com.pjatk.mylib.repo.BookRepo;
import com.pjatk.mylib.repo.ReservationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    private BookRepo bookRepo;

    @Autowired
    private ReservationRepo reservationRepo;

    public Book find(Integer id) {
        return bookRepo.find(id);
    }

    public List<Book> findByAuthor(String name, String surname) {
        return bookRepo.findByAuthor(name, surname);
    }

    public List<Book> findByTitle(String title) {
        return bookRepo.findByTitle(title);
    }

    public List<Book> findByIsbn(String isbn) {
        return bookRepo.findByIsbn(isbn);
    }

    public List<Book> findByPublisher(String publisher) {
        return bookRepo.findByPublisher(publisher);
    }

    public List<Book> findBySignature(String signature) {
        return bookRepo.findBySignature(signature);
    }

    public List<BookCopies> findAmountOfFreeCopies(Integer bookId) {
        return bookRepo.findAmountOfFreeCopies(bookId);
    }

    public boolean decreaseAvailableCopies(Integer bookId, Integer libraryId) {
        return bookRepo.decreaseAvailableCopies(bookId, libraryId);
    }

    public List<Reservation> getAllReservations(Integer idUser) {
        return reservationRepo.getAllReservations(idUser);
    }

    public boolean insertReservation(Reservation reservation) {
        return reservationRepo.insertReservation(reservation);
    }
}
