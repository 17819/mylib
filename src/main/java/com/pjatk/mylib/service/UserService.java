package com.pjatk.mylib.service;

import com.pjatk.mylib.model.User;
import com.pjatk.mylib.repo.UserRepo;
import com.pjatk.mylib.validation.UserValidator;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private Integer passwordMaxLength = 25;
    private Integer passwordMinLength = 12;
    private Integer usernameHashLength = 4;

    @Autowired
    private UserRepo userRepo;

    public User find(Integer id) {
        return userRepo.find(id);
    }

    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    public User findByPesel(String pesel) {
        return userRepo.findByPesel(pesel);
    }

    public List<String> findUserRoles(String username) {
        return userRepo.findUserRoles(username);
    }

    public boolean checkIfUserWithPeselExists(String pesel) {
        return (userRepo.findByPesel(pesel) != null);
    }

    public boolean updateUserPassword(String username, String password) {
        return userRepo.updateUserPassword(username, password);
    }

    public boolean checkIfValidOldPassword(User user, String password) {
        if (user == null) return false;
        return user.getPassword().equals(password);
    }

    public boolean checkIfNewPasswordsAreValid(String oldPassword, String password, String passwordConfirm) {
        return UserValidator.checkIfNewPasswordsAreValid(oldPassword, password, passwordConfirm, passwordMinLength, passwordMaxLength);
    }

    public boolean updateUserEmail(String username, String email) {
        return userRepo.updateUserEmail(username, email);
    }

    public boolean checkIfEmailsAreCorrect(String email, String emailConfirm) {
        return UserValidator.checkIfEmailsAreCorrect(email, emailConfirm);
    }

    /**
     * This method is responsible for creating new User
     * @param user model object, which contains user data
     * @return <strong>TRUE</strong> if user was inserted in successful way<br>
     *     <strong>FALSE</strong> if operation failed
     */
    public boolean insertUser(User user) {
        return userRepo.insertUser(user);
    }

    public boolean updateUser(User user) {
        return userRepo.updateUser(user);
    }

    /**
     * This method is used to generate random password for new user.<br>
     * Password will consist of upper/lower case letters and numbers.
     * @param length length of password
     * @return String with random characters.
     */
    public String generateRandomPassword(int length) {
        return RandomStringUtils.random(length, true, true);
    }

    public String generateUsername(int hashLength) {
        String username = "U";
        username += (userRepo.getLastId() + 1001);
        username += generateRandomPassword(hashLength);
        return username;
    }

    public User assignCredentialsToUser(User user) {
        user.setPassword(generateRandomPassword(passwordMinLength));
        user.setUsername(generateUsername(usernameHashLength));
        user.setEnabled(true);
        return user;
    }

    public boolean assignUserRole(String username) {
        return userRepo.assignUserRole(username);
    }

    public boolean assignEmployeeRole(String username) {
        return userRepo.assignEmployeeRole(username);
    }

      public boolean removeUser(String username) {
        return userRepo.removeUser(username);
    }
}
