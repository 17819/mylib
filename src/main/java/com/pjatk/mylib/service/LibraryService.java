package com.pjatk.mylib.service;

import com.pjatk.mylib.model.Library;
import com.pjatk.mylib.model.WorkingHours;
import com.pjatk.mylib.repo.LibraryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibraryService {
    @Autowired
    LibraryRepo libraryRepo;

    public List<Library> findAllLibraries() {
        return libraryRepo.findAllLibraries();
    }

    public List<WorkingHours> findAllWorkingHours() {
        return libraryRepo.findAllWorkingHours();
    }
}
