package com.pjatk.mylib.config;

import com.pjatk.mylib.constants.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    DataSource dataSource;

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery(
                        "select username, password, enabled from user where username=?")
                .authoritiesByUsernameQuery(
                        "select username, role from user_roles where username=?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterBefore(getCharacterEncodingFilter(), CsrfFilter.class);

        http.authorizeRequests()
                .antMatchers("/account/**").hasRole(Const.USER)
                .antMatchers("/books/order/**").hasRole(Const.USER)
                .antMatchers("/user/management/**").hasRole(Const.EMPLOYEE)
                .antMatchers("/employee/management/**").hasRole(Const.ADMIN)
            .and()
                .formLogin().loginPage("/login").failureUrl("/login?msgError").usernameParameter("username").passwordParameter("password")
            .and()
                .logout().logoutSuccessUrl("/login?logout").logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            .and()
                .exceptionHandling().accessDeniedPage("/errors/403")
            .and()
                .csrf();
    }

    /**
     * This filter must be set before {@link org.springframework.security.web.csrf.CsrfFilter}
     * in order to prevent issues with UTF-8 encoding for request parameters.<br><br>
     * See: <a href="https://stackoverflow.com/questions/20863489/characterencodingfilter-dont-work-together-with-spring-security-3-2-0">Solution</a>
     * @return
     */
    public CharacterEncodingFilter getCharacterEncodingFilter() {
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        return filter;
    }
}
