package com.pjatk.mylib.validation;

import com.pjatk.mylib.constants.Const;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.util.StringUtils;

import java.util.List;

public class UserValidator {
    public static boolean checkIfNewPasswordsAreValid(String oldPassword, String password, String passwordConfirm,
                                               Integer passwordMinLength, Integer passwordMaxLength) {
        if (password == null) return false;
        if (passwordConfirm == null) return false;
        if (StringUtils.containsWhitespace(password)) return false;
        if (StringUtils.containsWhitespace(passwordConfirm)) return false;
        if (password.length() < passwordMinLength) return false;
        if (password.length() > passwordMaxLength) return false;
        if (oldPassword.equals(password)) return false;
        return password.equals(passwordConfirm);
    }

    public static boolean checkIfEmailsAreCorrect(String email, String emailConfirm) {
        if (email == null || emailConfirm == null) return false;
        if (!email.equals(emailConfirm)) return false;
        return EmailValidator.getInstance().isValid(email);
    }

    public static boolean checkIfUserIsAdminOrEmployee(List<String> roles) {
        if (roles == null) {
            return false;
        }
        for (String role : roles) {
            if (Const.ROLE_ADMIN.equals(role) || Const.ROLE_EMPLOYEE.equals(role)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfUserIsAdmin(List<String> roles) {
        if (roles == null) {
            return false;
        }
        for (String role : roles) {
            if (Const.ROLE_ADMIN.equals(role)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfUserIsEmployee(List<String> roles) {
        if (roles == null) {
            return false;
        }
        for (String role : roles) {
            if (Const.ROLE_EMPLOYEE.equals(role)) {
                return true;
            }
        }
        return false;
    }
}
