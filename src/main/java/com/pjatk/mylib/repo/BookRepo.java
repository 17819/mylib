package com.pjatk.mylib.repo;

import com.pjatk.mylib.model.Book;
import com.pjatk.mylib.model.BookCopies;

import java.util.List;

public interface BookRepo {
    Book find(Integer id);

    List<Book> findByAuthor(String name, String surname);

    List<Book> findByTitle(String title);

    List<Book> findByIsbn(String isbn);

    List<Book> findByPublisher(String publisher);

    List<Book> findBySignature(String signature);

    List<BookCopies> findAmountOfFreeCopies(Integer bookId);

    boolean decreaseAvailableCopies(Integer bookId, Integer libraryId);
}
