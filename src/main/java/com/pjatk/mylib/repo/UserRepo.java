package com.pjatk.mylib.repo;

import com.pjatk.mylib.model.User;

import java.util.List;

public interface UserRepo {
    User find(Integer id);

    User findByUsername(String username);

    User findByPesel(String pesel);

    List<String> findUserRoles(String username);

    boolean updateUserPassword(String username, String password);

    boolean updateUserEmail(String username, String email);

    boolean insertUser(User user);

    boolean updateUser(User user);

    Integer getLastId();

    boolean assignUserRole(String username);

    boolean assignEmployeeRole(String username);

    boolean removeUser(String username);
}
