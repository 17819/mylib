package com.pjatk.mylib.repo;

import com.pjatk.mylib.model.Library;
import com.pjatk.mylib.model.WorkingHours;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class LibraryRepoImpl implements LibraryRepo{
    @PersistenceContext(unitName = "library")
    EntityManager em;

    @Override
    public List<Library> findAllLibraries() {
        return em.createNamedQuery(Library.FIND_ALL_LIBRARIES, Library.class).getResultList();
    }

    @Override
    public List<WorkingHours> findAllWorkingHours() {
        return em.createNamedQuery(WorkingHours.FIND_ALL, WorkingHours.class).getResultList();
    }
}
