package com.pjatk.mylib.repo;

import com.pjatk.mylib.model.Book;
import com.pjatk.mylib.model.BookCopies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class BookRepoImpl implements BookRepo {
    Logger logger = LoggerFactory.getLogger(BookRepoImpl.class);

    @PersistenceContext(unitName = "library")
    EntityManager em;

    @Override
    public Book find(Integer id) {
        return em.find(Book.class, id);
    }

    @Override
    public List<Book> findByAuthor(String name, String surname) {
        Query q = em.createNamedQuery(Book.FIND_BOOKS_BY_AUTHOR_ASC, Book.class);
        q.setParameter(1, name);
        q.setParameter(2, surname);
        return q.getResultList();
    }

    @Override
    public List<Book> findByTitle(String title) {
        Query q = em.createNamedQuery(Book.FIND_BOOKS_BY_TITLE_ASC, Book.class);
        q.setParameter(1, title);
        return q.getResultList();
    }

    @Override
    public List<Book> findByIsbn(String isbn) {
        Query q = em.createNamedQuery(Book.FIND_BOOKS_BY_ISBN_ASC, Book.class);
        q.setParameter(1, isbn);
        return q.getResultList();
    }

    @Override
    public List<Book> findByPublisher(String publisher) {
        Query q = em.createNamedQuery(Book.FIND_BOOKS_BY_PUBLISHER_ASC, Book.class);
        q.setParameter(1, publisher);
        return q.getResultList();
    }

    @Override
    public List<Book> findBySignature(String signature) {
        Query q = em.createNamedQuery(Book.FIND_BOOKS_BY_SIGNATURE_ASC, Book.class);
        q.setParameter(1, signature);
        return q.getResultList();
    }

    @Override
    public List<BookCopies> findAmountOfFreeCopies(Integer bookId) {
        Query q = em.createNamedQuery(BookCopies.FIND_ALL_FREE_COPIES, BookCopies.class);
        q.setParameter(1, bookId);
        return q.getResultList();
    }

    @Override
    @Transactional
    public boolean decreaseAvailableCopies(Integer bookId, Integer libraryId) {
        try {
            Query q = em.createNamedQuery(BookCopies.DECREASE_AVAILABLE_COPIES);
            q.setParameter(1, bookId);
            q.setParameter(2, libraryId);
            q.executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error("BookRepoImpl: Failed to reduce available book copies", e);
            return false;
        }
    }
}
