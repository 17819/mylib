package com.pjatk.mylib.repo;

import com.pjatk.mylib.model.Library;
import com.pjatk.mylib.model.WorkingHours;

import java.util.List;

public interface LibraryRepo {
    List<Library> findAllLibraries();

    List<WorkingHours> findAllWorkingHours();
}
