package com.pjatk.mylib.repo;

import com.pjatk.mylib.model.Reservation;

import java.util.List;

public interface ReservationRepo {
    boolean insertReservation(Reservation reservation);

    List<Reservation> getAllReservations(Integer idUser);
}
