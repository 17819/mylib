package com.pjatk.mylib.repo;

import com.pjatk.mylib.model.Reservation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ReservationRepoImpl implements ReservationRepo {
    Logger logger = LoggerFactory.getLogger(UserRepoImpl.class);

    @PersistenceContext(unitName = "library")
    EntityManager em;

    @Override
    @Transactional
    public boolean insertReservation(Reservation reservation) {
        if (reservation == null) return false;
        try {
            em.persist(reservation);
            return true;
        } catch (Exception e) {
            logger.error("ReservationRepoImpl: Error during insertion of reservation", e);
            return false;
        }
    }

    @Override
    public List<Reservation> getAllReservations(Integer idUser) {
        Query q = em.createNamedQuery(Reservation.FIND_ALL_BY_USER_ID, Reservation.class);
        q.setParameter(1, idUser);
        return q.getResultList();
    }
}
