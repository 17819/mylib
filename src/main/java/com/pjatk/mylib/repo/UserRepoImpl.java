package com.pjatk.mylib.repo;

import com.pjatk.mylib.constants.Const;
import com.pjatk.mylib.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class UserRepoImpl implements UserRepo {
    Logger logger = LoggerFactory.getLogger(UserRepoImpl.class);

    @PersistenceContext(unitName = "library")
    EntityManager em;

    @Override
    public User find(Integer id) {
        return em.find(User.class, id);
    }

    @Override
    public User findByUsername(String username) {
        Query q = em.createNamedQuery(User.FIND_BY_USERNAME, User.class);
        q.setParameter(1, username);
        try {
            return (User) q.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    @Override
    public User findByPesel(String pesel) {
        Query q = em.createNamedQuery(User.FIND_BY_PESEL, User.class);
        q.setParameter(1, pesel);
        try {
            return (User) q.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    @Override
    public List<String> findUserRoles(String username) {
        List<String> roles;
        Query q = em.createNamedQuery(User.FIND_ALL_USER_ROLES);
        q.setParameter(1, username);
        roles = q.getResultList();
        return roles;
    }

    @Override
    @Transactional
    public boolean updateUserPassword(String username, String password) {
        try {
            Query q = em.createNamedQuery(User.UPDATE_USER_PASSWORD, User.class);
            q.setParameter(1, password);
            q.setParameter(2, username);
            q.executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error("UserRepoImpl: Error during update of user password", e);
            return false;
        }
    }

    @Override
    @Transactional
    public boolean updateUserEmail(String username, String email) {
        try {
            Query q = em.createNamedQuery(User.UPDATE_USER_EMAIL, User.class);
            q.setParameter(1, email);
            q.setParameter(2, username);
            q.executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error("UserRepoImpl: Error during update of user email", e);
            return false;
        }
    }

    @Override
    @Transactional
    public boolean insertUser(User user) {
        if (user == null) return false;
        try {
            em.persist(user);
            return true;
        } catch (Exception e) {
            logger.error("UserRepoImpl: Error during insertion of user", e);
            return false;
        }
    }

    @Override
    public Integer getLastId() {
        try {
            return (Integer) em.createNamedQuery(User.GET_LAST_ID).getSingleResult();
        } catch (NoResultException nre) {
            logger.error("UserRepoImpl: Error during search for user id", nre);
            return null;
        }
    }

    @Override
    @Transactional
    public boolean assignUserRole(String username) {
        try {
            Query q = em.createNamedQuery(User.ASSIGN_USER_ROLE);
            q.setParameter(1, username);
            q.setParameter(2, Const.ROLE_USER);
            q.executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error("UserRepoImpl: Error during insertion of user role", e);
            return false;
        }
    }

    @Override
    @Transactional
    public boolean assignEmployeeRole(String username) {
        try {
            Query q = em.createNamedQuery(User.ASSIGN_USER_ROLE);
            q.setParameter(1, username);
            q.setParameter(2, Const.ROLE_EMPLOYEE);
            q.executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error("UserRepoImpl: Error during insertion of employee role", e);
            return false;
        }
    }

    @Override
    @Transactional
    public boolean removeUser(String username) {
        try {
            Query q = em.createNamedQuery(User.DELETE_USER);
            q.setParameter(1, username);
            q.executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error("UserRepoImpl: Failed to delete user", e);
            return false;
        }
    }

    @Override
    @Transactional
    public boolean updateUser(User user) {
        try {
            Query q = em.createNamedQuery(User.UPDATE_USER);
            q.setParameter(1, user.getName());
            q.setParameter(2, user.getSurname());
            q.setParameter(3, user.getPesel());
            q.setParameter(4, user.getEmail());
            q.setParameter(5, user.getSocialStatus());
            q.setParameter(6, user.getBirthDate());
            q.setParameter(7, user.getCity());
            q.setParameter(8, user.getStreet());
            q.setParameter(9, user.getHouseNumber());
            q.setParameter(10, user.getApartmentNumber());
            q.setParameter(11, user.getPostalCode());
            q.setParameter(12, user.getUsername());
            q.executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error("UserRepoImpl: Failed to update user", e);
            return false;
        }
    }
}
