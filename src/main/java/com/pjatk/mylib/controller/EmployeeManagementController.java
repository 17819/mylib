package com.pjatk.mylib.controller;

import com.pjatk.mylib.constants.Const;
import com.pjatk.mylib.constants.RequestParams;
import com.pjatk.mylib.model.User;
import com.pjatk.mylib.service.UserService;
import com.pjatk.mylib.validation.UserValidator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/employee/management")
public class EmployeeManagementController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String goToCreateUser(ModelMap model) {
        model.addAttribute(Const.MODEL_USER, new User());
        return "employee/insertEmployee";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createUser(@ModelAttribute User user,
                             ModelMap model) {

        boolean isErrorFound = false;
        if (userService.checkIfUserWithPeselExists(user.getPesel())) {
            model.put(Const.MESSAGE_ERROR, "Użytkownik z podanym numerem PESEL istnieje");
            isErrorFound = true;
        }

        if (!isErrorFound && !userService.checkIfEmailsAreCorrect(user.getEmail(), user.getEmail())) {
            model.put(Const.MESSAGE_ERROR, "Podany adres email jest nieprawidłowy");
            isErrorFound = true;
        }

        if (!isErrorFound) {
            userService.assignCredentialsToUser(user);
            if (userService.insertUser(user)) {
                model.put(Const.MESSAGE_SUCCESS, "Konto pracownika zostało poprawnie założone");
                if (userService.assignUserRole(user.getUsername()) && userService.assignEmployeeRole(user.getUsername())) {
                    model.put(Const.MESSAGE_INFO, "Przypisano czytelnikowi role: Użytkownik oraz Pracownik");
                } else {
                    model.put(Const.MESSAGE_ERROR, "Wystąpił nieznany błąd podczas przypisywania roli");
                }
            } else {
                model.put(Const.MESSAGE_ERROR, "Wystąpił nieznany błąd podczas tworzenia konta czytelnika");
            }
        }

        return "employee/insertEmployee";
    }

    @RequestMapping(value = "/delete/{username}", method = RequestMethod.GET)
    public String deleteUser(ModelMap model, @PathVariable("username") String username) {
        User user = null;
        if (username != null) {
            user = userService.findByUsername(username);
        }

        if (user != null) {
            if (!UserValidator.checkIfUserIsAdmin(userService.findUserRoles(username))) {
                if (!userService.removeUser(username)) {
                    model.put(Const.MESSAGE_ERROR, "Wystąpił błąd poczas usuwania konta pracownika");
                } else {
                    model.put(Const.MESSAGE_SUCCESS, "Poprawnie usunięto pracownika");
                }
            } else {
                model.put(Const.MESSAGE_ERROR, "Brak uprawnień do usunięcia konta");
            }
        } else {
            model.put(Const.MESSAGE_ERROR, "Nie znaleziono pracownika");
        }
        return "employee/menu";
    }

    @RequestMapping(value = "/modify/{username}", method = RequestMethod.GET)
    public String goToModifyUser(ModelMap model, @PathVariable("username") String username) {
        User user = userService.findByUsername(username);
        model.put(Const.MODEL_USER, user);
        model.put(Const.SOCIAL_STATUS_VALUE_TO_SELECT, user.getSocialStatus());
        return "employee/modifyEmployee";
    }

    @RequestMapping(value = "/modify/{username}", method = RequestMethod.POST)
    public String modifyUser(ModelMap model, @ModelAttribute User user, @PathVariable("username") String username) {
        boolean isErrorFound = false;
        user.setUsername(username);

        if (!userService.checkIfEmailsAreCorrect(user.getEmail(), user.getEmail())) {
            model.put(Const.MESSAGE_ERROR, "Podany adres email jest nieprawidłowy");
            isErrorFound = true;
        }

        if (!isErrorFound && userService.updateUser(user)) {
            model.put(Const.MESSAGE_SUCCESS, "Zaktualizowano dane czytelnika");
        } else {
            model.put(Const.MESSAGE_ERROR, "Wystąpił problem z aktualizacją danych");
        }
        return "employee/menu";
    }
    @RequestMapping(value = "/menu", method = RequestMethod.GET)
    public String goToMenu(ModelMap model) {
        return "employee/menu";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String goToSearchUser(ModelMap model) {
        return "employee/searchEmployee";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchUser(@RequestParam(value = RequestParams.USERNAME) String username,
                             @RequestParam(value = RequestParams.PESEL) String pesel,
                             ModelMap model) {
        User user = null;
        if (StringUtils.isNotEmpty(username)) {
            user = userService.findByUsername(username);
        } else if (StringUtils.isNotEmpty(pesel)) {
            user = userService.findByPesel(pesel);
        }

        if (user != null) {
            List<String> roles = userService.findUserRoles(user.getUsername());
            if (!UserValidator.checkIfUserIsAdmin(roles) && UserValidator.checkIfUserIsEmployee(roles)) {
                model.put(Const.MODEL_USER, user);
            } else {
                model.put(Const.MESSAGE_ERROR, "Brak uprawnień do wyświetlenia danych konta");
            }
        } else {
            model.put(Const.MESSAGE_ERROR, "Nie znaleziono pracownika");
        }

        return "employee/searchEmployee";
    }

    /**
     * Adding this binder prevents from this issue:
     * <a href="https://stackoverflow.com/questions/34803874/spring-mvc-http-status-400-bad-request">link</a>
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}
