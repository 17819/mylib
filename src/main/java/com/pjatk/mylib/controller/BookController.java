package com.pjatk.mylib.controller;

import com.pjatk.mylib.constants.Const;
import com.pjatk.mylib.constants.RequestParams;
import com.pjatk.mylib.model.Book;
import com.pjatk.mylib.model.BookCopies;
import com.pjatk.mylib.model.Reservation;
import com.pjatk.mylib.model.User;
import com.pjatk.mylib.service.BookService;
import com.pjatk.mylib.service.UserService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/books")
public class BookController {
    @Autowired
    BookService bookService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    private String findBooks(@RequestParam(value = RequestParams.AUTHOR_NAME, required=false) String name,
                             @RequestParam(value = RequestParams.AUTHOR_SURNAME, required=false) String surname,
                             @RequestParam(value = RequestParams.TITLE, required=false) String title,
                             @RequestParam(value = RequestParams.SIGNATURE, required=false) String signature,
                             @RequestParam(value = RequestParams.ISBN, required=false) String isbn,
                             @RequestParam(value = RequestParams.PUBLISHER, required=false) String publisher,
                             @RequestParam(value = RequestParams.RADIO_SEARCH_TYPE, required=false) String searchType,
                                         ModelMap model) {

        List<Book> result;

        if (Const.RADIO_VALUE_BY_AUTHOR.equals(searchType)) {
            result = bookService.findByAuthor(name, surname);
            model.put(Const.CLICKED_RADIO_BUTTON_ID, "radioAuthor");
        } else if (Const.RADIO_VALUE_BY_TITLE.equals(searchType)) {
            result = bookService.findByTitle(title);
            model.put(Const.CLICKED_RADIO_BUTTON_ID, "radioTitle");
        } else if (Const.RADIO_VALUE_BY_PUBLISHER.equals(searchType)) {
            result = bookService.findByPublisher(publisher);
            model.put(Const.CLICKED_RADIO_BUTTON_ID, "radioPublisher");
        } else if (Const.RADIO_VALUE_BY_SIGNATURE.equals(searchType)) {
            result = bookService.findBySignature(signature);
            model.put(Const.CLICKED_RADIO_BUTTON_ID, "radioSignature");
        } else if (Const.RADIO_VALUE_BY_ISBN.equals(searchType)) {
            result = bookService.findByIsbn(isbn);
            model.put(Const.CLICKED_RADIO_BUTTON_ID, "radioISBN");
        } else {
            result = new ArrayList<>();
        }

        if (!result.isEmpty()) {
            model.put(Const.LIST_OF_BOOKS, result);
        }

        return "books/search";
    }

    @RequestMapping("/{id}/show")
    private String show(ModelMap model, @PathVariable("id") Integer id) {
        Book book = bookService.find(id);
        if (book == null) {
            book = new Book();
        } else {
            List<BookCopies> copies = bookService.findAmountOfFreeCopies(book.getId());
            model.put(Const.LIST_OF_COPIES, copies);
        }
        model.put(Const.MESSAGE_INFO, "Aby móc wypożyczyć książkę, musisz się uprzednio zalogować");
        model.put(Const.MODEL_BOOK, book);
        return "books/show";
    }

    @RequestMapping(value = "/order/{bookId}/{libraryId}", method = RequestMethod.GET)
    public String reserveBook(Principal principal,
                              ModelMap model,
                              @PathVariable("bookId") Integer bookId,
                              @PathVariable("libraryId") Integer libraryId) {
        String userName = principal.getName();
        List<BookCopies> copies = bookService.findAmountOfFreeCopies(bookId);
        if (copies != null) {
            int freeCopies = copies.get(libraryId - 1).getFree();
            if (freeCopies < 1) {
                model.put(Const.MESSAGE_ERROR, "Nie znaleziono wolnych egzemplarzy");
            } else {
                User user = userService.findByUsername(userName);
                Date now = new Date();
                Date nextDate = DateUtils.addMonths(new Date(), 1);

                Reservation reservation = new Reservation();
                reservation.setIdLibrary(libraryId);
                reservation.setIdBook(bookId);
                reservation.setIdUser(user.getId());
                reservation.setRentalDate(now);
                reservation.setReturnDate(nextDate);
                boolean result = bookService.insertReservation(reservation);
                if (result == true) {
                    bookService.decreaseAvailableCopies(bookId, libraryId);
                    model.put(Const.MESSAGE_SUCCESS, "Pomyślnie zarezerwowano ksiązkę");
                } else {
                    model.put(Const.MESSAGE_ERROR, "Wystąpił problem podczas zamawiania egzamplarza książki");
                }
            }
        } else {
            model.put(Const.MESSAGE_ERROR, "Nie znaleziono wolnych egzemplarzy");
        }
        return show(model, bookId);
    }
}
