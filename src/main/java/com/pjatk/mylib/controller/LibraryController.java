package com.pjatk.mylib.controller;

import com.pjatk.mylib.constants.Const;
import com.pjatk.mylib.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/library")
public class LibraryController {
    @Autowired
    LibraryService libraryService;

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public String login(ModelMap model) {
        model.put(Const.LIST_OF_LIBRARIES, libraryService.findAllLibraries());
        model.put(Const.WORKING_HOURS, libraryService.findAllWorkingHours());
        return "library/info";
    }
}
