package com.pjatk.mylib.controller;

import com.pjatk.mylib.constants.Const;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {
    @RequestMapping(value = "/errors/403", method = RequestMethod.GET)
    public String accesssDenied(ModelMap model) {
        return "error/403";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(@RequestParam(value = "msgError", required = false) String error,
                        @RequestParam(value = "logout", required = false) String logout,
                        ModelMap model) {
        if (error != null) {
            model.put(Const.MESSAGE_ERROR, "Niepoprawna nazwa użytkownika lub hasło!");
        }
        if (logout != null) {
            model.put(Const.MESSAGE_SUCCESS, "Zostałeś pomyślnie wylogowany/a.");
        }

        return "auth/login";
    }
}
