package com.pjatk.mylib.controller;

import com.pjatk.mylib.constants.Const;
import com.pjatk.mylib.constants.RequestParams;
import com.pjatk.mylib.model.Reservation;
import com.pjatk.mylib.model.User;
import com.pjatk.mylib.service.BookService;
import com.pjatk.mylib.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/account")
public class UserController {
    @Autowired
    UserService userService;

    @Autowired
    BookService bookService;

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public String accountInfo(Principal principal, ModelMap model) {
        String username = principal.getName();
        User user = userService.findByUsername(username);
        if (user == null) user = new User();
        model.put(Const.MODEL_USER, user);
        return "account/info";
    }

    @RequestMapping(value = "/changepass", method = RequestMethod.GET)
    public String goToChangePasswordForm(ModelMap model) {
        return "account/changepass";
    }

    @RequestMapping(value = "/reservations", method = RequestMethod.GET)
    public String goToReservationsPasswordForm(Principal principal,
                                               ModelMap model) {
        String userName = principal.getName();
        User user = userService.findByUsername(userName);
        if (user != null) {
            List<Reservation> reservationList = bookService.getAllReservations(user.getId());
            if (reservationList.size() == 0) model.put(Const.MESSAGE_INFO, "Brak rezerwacji na koncie");
            model.put(Const.LIST_OF_RESERVATIONS, reservationList);
        }
        return "account/reservations";
    }

    @RequestMapping(value = "/changepass", method = RequestMethod.POST)
    public String changePassword(@RequestParam(RequestParams.OLD_PASSOWRD) String oldPassword,
                                 @RequestParam(RequestParams.PASSWORD) String password,
                                 @RequestParam(RequestParams.PASSWORD_CONFIRMATION) String passwordConfirm,
                                 Principal principal, ModelMap model) {
        String username = principal.getName();
        boolean isErrorFound = false;

        if (!checkIfPasswordIsValid(username, oldPassword)) {
            model.put(Const.MESSAGE_ERROR, "Stare hasło jest niepoprawne");
            isErrorFound = true;
        }

        if (!isErrorFound && !userService.checkIfNewPasswordsAreValid(oldPassword, password, passwordConfirm)) {
            model.put(Const.MESSAGE_ERROR, "Nowe hasła zostały wprowadzone nieprawidłowo");
            isErrorFound = true;
        }

        if (!isErrorFound) {
            if (userService.updateUserPassword(username, password)) {
                model.put(Const.MESSAGE_SUCCESS, "Hasło zostało zmienione poprawnie");
            } else {
                model.put(Const.MESSAGE_ERROR, "Wystąpił nieznany błąd podczas zmiany hasła");
            }
        }

        return "account/changepass";
    }

    @RequestMapping(value = "/changemail", method = RequestMethod.GET)
    public String goToChangeMailForm(ModelMap model) {
        return "account/changemail";
    }

    @RequestMapping(value = "/changemail", method = RequestMethod.POST)
    public String changeEmail(@RequestParam(RequestParams.EMAIL) String email,
                                 @RequestParam(RequestParams.EMAIL_CONFIRMATION) String emailConfirm,
                                 @RequestParam(RequestParams.PASSWORD) String password,
                                 Principal principal, ModelMap model) {
        String username = principal.getName();
        boolean isErrorFound = false;

        if (!checkIfPasswordIsValid(username, password)) {
            model.put(Const.MESSAGE_ERROR, "Stare hasło jest niepoprawne");
            isErrorFound = true;
        }

        if (!isErrorFound && !userService.checkIfEmailsAreCorrect(email, emailConfirm)) {
            model.put(Const.MESSAGE_ERROR, "Wprowadzone adresy email są niepoprawne");
            isErrorFound = true;
        }

        if (!isErrorFound) {
            if (userService.updateUserEmail(username, email)) {
                model.put(Const.MESSAGE_SUCCESS, "Adres email został zmieniony poprawnie");
            } else {
                model.put(Const.MESSAGE_ERROR, "Wystąpił nieznany błąd podczas zmiany adresu mailowego");
            }
        }

        return "account/changemail";
    }

    protected boolean checkIfPasswordIsValid(String username, String password) {
        User user = userService.findByUsername(username);
        return userService.checkIfValidOldPassword(user, password);
    }
}
