package com.pjatk.mylib.constants;

/**
 * This class contains various constants used in application
 */
public class Const {
    //MODEL CONSTANTS
    public static final String MODEL_BOOK = "book";
    public static final String MODEL_USER = "user";
    public static final String LIST_OF_BOOKS = "books";
    public static final String LIST_OF_LIBRARIES = "libraries";
    public static final String LIST_OF_COPIES = "copies";
    public static final String LIST_OF_RESERVATIONS = "reservations";
    public static final String WORKING_HOURS = "workingHours";

    //VALIDATION MESSAGES
    public static final String MESSAGE_SUCCESS = "msgSuccess";
    public static final String MESSAGE_WARNING = "msgWarning";
    public static final String MESSAGE_ERROR = "msgError";
    public static final String MESSAGE_INFO = "msgInfo";

    //RADIO BUTTONS
    public static final String CLICKED_RADIO_BUTTON_ID = "clickedRadioButton";
    public static final String RADIO_VALUE_BY_AUTHOR = "byAuthor";
    public static final String RADIO_VALUE_BY_TITLE = "byTitle";
    public static final String RADIO_VALUE_BY_SIGNATURE = "bySignature";
    public static final String RADIO_VALUE_BY_ISBN = "byISBN";
    public static final String RADIO_VALUE_BY_PUBLISHER = "byPublisher";
    public static final String RADIO_ORDER_ASCENDING = "ASC";
    public static final String RADIO_ORDER_DESCENDING = "DESC";

    //SELECT
    public static final String SOCIAL_STATUS_VALUE_TO_SELECT = "socialStatusValue";

    //SOCIAL STATUS CONSTANTS
    public static final String SOCIAL_STATUS_STUDENT = "ST";
    public static final String SOCIAL_STATUS_PUPIL = "UC";
    public static final String SOCIAL_STATUS_UNEMPLOYED = "UN";
    public static final String SOCIAL_STATUS_EMPLOYED_PERSON = "EP";

    //ROLES
    public static final String ROLE_USER = "ROLE_USER";
    public static final String USER = "USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ADMIN = "ADMIN";
    public static final String ROLE_EMPLOYEE = "ROLE_EMPLOYEE";
    public static final String EMPLOYEE = "EMPLOYEE";
}
