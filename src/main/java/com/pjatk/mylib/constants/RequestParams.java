package com.pjatk.mylib.constants;

/**
 * This class contains request parameters constants, which are reusable within application.<br>
 * Using constants makes it easier to work with ambiguous parameter names.
 */
public class RequestParams {
    //BOOK
    public static final String AUTHOR_NAME = "name";
    public static final String AUTHOR_SURNAME = "surname";
    public static final String TITLE = "title";
    public static final String SIGNATURE = "signature";
    public static final String ISBN = "isbn";
    public static final String PUBLISHER = "publisher";

    //USER
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String PESEL = "pesel";
    public static final String USERNAME = "username";
    public static final String SOCIAL_STATUS = "socialStatus";
    public static final String BIRTH_DATE = "birthDate";
    public static final String CITY = "city";
    public static final String POSTAL_CODE = "postalCode";
    public static final String STREET = "street";
    public static final String HOUSE_NUMBER = "houseNumber";
    public static final String APARTMENT_NUMBER = "apartmentNumber";

    public static final String EMAIL = "email";
    public static final String EMAIL_CONFIRMATION = "emailConfirm";
    public static final String PASSWORD = "password";
    public static final String PASSWORD_CONFIRMATION = "passwordConfirm";
    public static final String OLD_PASSOWRD = "oldPassword";

    //FORM
    public static final String RADIO_SEARCH_TYPE = "radioType";
}
