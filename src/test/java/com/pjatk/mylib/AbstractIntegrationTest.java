package com.pjatk.mylib;

import com.pjatk.mylib.config.TestConfig;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringJUnit4ClassRunner.class)
@Ignore
@Transactional(readOnly = false)
@Rollback
@ContextConfiguration(classes = TestConfig.class)
public class AbstractIntegrationTest {
    @PersistenceContext
    private EntityManager em;
}
