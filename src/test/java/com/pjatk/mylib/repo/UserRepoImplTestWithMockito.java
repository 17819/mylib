package com.pjatk.mylib.repo;

import com.pjatk.mylib.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

@RunWith(MockitoJUnitRunner.class)
public class UserRepoImplTestWithMockito {
    @Mock
    private EntityManager entityManager;

    @InjectMocks
    private UserRepoImpl userRepo;

    @Test
    public void shouldThrowExceptionAndReturnFalseDuringEmailUpdate() {
        //given

        //when
        Mockito.when(entityManager.createNamedQuery(User.UPDATE_USER_EMAIL, User.class)).thenThrow(Exception.class);
        boolean result = userRepo.updateUserEmail("someEmail", "someEmail");

        //then
        assertFalse(result);
    }

    @Test
    public void shouldThrowExceptionAndReturnFalseDuringPasswordUpdate() {
        //given

        //when
        Mockito.when(entityManager.createNamedQuery(User.UPDATE_USER_PASSWORD, User.class)).thenThrow(Exception.class);
        boolean result = userRepo.updateUserPassword("someUsername", "somePassword");

        //then
        assertFalse(result);
    }

    @Test
    public void shouldThrowExceptionAndReturnNullWhenSearchingForLastId() {
        //given

        //when
        Mockito.when(entityManager.createNamedQuery(User.GET_LAST_ID)).thenThrow(NoResultException.class);
        Integer result = userRepo.getLastId();

        //then
        assertNull(result);
    }

    @Test
    public void shouldThrowExceptionAndReturnFalseWhenAssigningUserRole() {
        //given

        //when
        Mockito.when(entityManager.createNamedQuery(User.ASSIGN_USER_ROLE)).thenThrow(Exception.class);
        boolean result = userRepo.assignUserRole("someUsername");

        //then
        assertFalse(result);
    }

    @Test
    public void shouldThrowExceptionAndReturnFalseWhenAssigningEmployeeRole() {
        //given

        //when
        Mockito.when(entityManager.createNamedQuery(User.ASSIGN_USER_ROLE)).thenThrow(Exception.class);
        boolean result = userRepo.assignEmployeeRole("someUsername");

        //then
        assertFalse(result);
    }

    @Test
    public void shouldThrowExceptionAndReturnFalseWhenRemovingUser() {
        //given

        //when
        Mockito.when(entityManager.createNamedQuery(User.DELETE_USER)).thenThrow(Exception.class);
        boolean result = userRepo.removeUser("someUsername");

        //then
        assertFalse(result);
    }

    @Test
    public void shouldThrowExceptionAndReturnFalseWhenUpdatingUser() {
        //given

        //when
        Mockito.when(entityManager.createNamedQuery(User.UPDATE_USER)).thenThrow(Exception.class);
        boolean result = userRepo.updateUser(new User());

        //then
        assertFalse(result);
    }
}
