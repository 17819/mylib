package com.pjatk.mylib.repo;

import com.pjatk.mylib.AbstractIntegrationTest;
import com.pjatk.mylib.model.Library;
import com.pjatk.mylib.model.WorkingHours;
import org.junit.Test;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;

public class LibraryRepoImplTest extends AbstractIntegrationTest {
    @Inject
    LibraryRepo libraryRepo;

    @Test
    public void shouldFindAllLibraries() {
        List<Library> result = libraryRepo.findAllLibraries();
        assertTrue(result.size() == 2);
    }

    @Test
    public void shouldFindAllWorkingHors() {
        List<WorkingHours> result = libraryRepo.findAllWorkingHours();
        assertTrue(result.size() == 7);
    }
}