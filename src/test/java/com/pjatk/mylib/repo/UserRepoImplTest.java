package com.pjatk.mylib.repo;

import com.pjatk.mylib.AbstractIntegrationTest;
import com.pjatk.mylib.constants.Const;
import com.pjatk.mylib.model.User;
import org.junit.Test;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;

public class UserRepoImplTest extends AbstractIntegrationTest {
    @Inject
    UserRepoImpl repo;

    @Test
    public void shouldNotFindAnyUserWhenSearchingById() throws Exception {
        // given
        Integer wrongId = 99999;

        // when
        User user = repo.find(wrongId);

        // then
        assertNull(user);
    }

    @Test
    public void shouldFindUserSearchingById() throws Exception {
        // given
        Integer id = 1;

        // when
        User user = repo.find(id);

        // then
        assertNotNull(user);
    }

    @Test
    public void shouldNotFindAnyUserWhenSearchingByUsername() throws Exception {
        // given
        String username = "TOTALLY WRONG USERNAME";

        // when
        User user = repo.findByUsername(username);

        // then
        assertNull(user);
    }

    @Test
    public void shouldFindUserSearchingByUsername() throws Exception {
        // given
        String username = "admin";

        // when
        User user = repo.findByUsername(username);

        // then
        assertNotNull(user);
        assertEquals(username, user.getUsername());
    }

    @Test
    public void shouldNotFindAnyUserWhenSearchingByPesel() throws Exception {
        // given
        String pesel = "TOTALLY WRONG PESEL";

        // when
        User user = repo.findByPesel(pesel);

        // then
        assertNull(user);
    }

    @Test
    public void shouldFindUserSearchingByPesel() throws Exception {
        // given
        String pesel = "12345678901";

        // when
        User user = repo.findByPesel(pesel);

        // then
        assertNotNull(user);
        assertEquals(pesel, user.getPesel());
    }

    @Test
    public void shouldNotFindUseRoles() throws Exception {
        // given
        String username = "TOTALLY WRONG USERNAME";

        // when
        List<String> roles = repo.findUserRoles(username);

        // then
        assertTrue(roles.isEmpty());
    }

    @Test
    public void shouldFindUseRoles() throws Exception {
        // given
        String username = "admin";

        // when
        List<String> roles = repo.findUserRoles(username);

        // then
        assertFalse(roles.isEmpty());
        assertTrue(roles.size() == 3);
    }

    @Test
    public void shouldUpdateUserPassword() throws Exception {
        //given
        String username = "admin";
        String currentPassword = "admin1";
        String newPassword = "superAdmin1";

        //when
        boolean result = repo.updateUserPassword(username, newPassword);
        User user = repo.findByUsername(username);

        //then
        assertTrue(result);
        assertEquals(newPassword, user.getPassword());
    }

    @Test
    public void shouldUpdateUserEmail() throws Exception {
        //given
        String username = "admin";
        String currentEmail = "aaa@wp.pl";
        String newEmail = "superaaa@wp.pl";

        //when
        boolean result = repo.updateUserEmail(username, newEmail);
        User user = repo.findByUsername(username);

        //then
        assertTrue(result);
        assertEquals(newEmail, user.getEmail());
    }

    @Test
    public void shouldNotInsertNullUser() {
        //given
        User user = null;

        //when
        boolean result = repo.insertUser(user);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldNotInsertUserWithoutRequiredData() {
        //given
        User user = new User();

        //when
        boolean result = repo.insertUser(user);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldInsertUser() {
        //given
        User user = new UserBuilder().buildTestUser();
        User afterInsert;

        //when
        boolean result = repo.insertUser(user);
        afterInsert = repo.findByUsername(user.getUsername());

        //then
        assertTrue(result);
        assertEquals("UnitTestUsername", afterInsert.getUsername());
    }

    @Test
    public void shouldReturnMaxId() {
        //given
        Integer expectedResult = 3;

        //when
        Integer maxId = repo.getLastId();

        //then
        assertEquals(expectedResult, maxId);
    }

    @Test
    public void shouldNotReturnMaxIdWhenNoDataInDB() {
        //given

        //when
        repo.removeUser("admin");
        repo.removeUser("emp");
        repo.removeUser("user");
        Integer result = repo.getLastId();

        //then
        assertNull(result);
    }

    @Test
    public void shouldRemoveUser() {
        //given
        String username = "admin";

        //when
        boolean result = repo.removeUser(username);
        User user = repo.findByUsername(username);

        //then
        assertTrue(result);
        assertNull(user);
    }

    @Test
    public void shouldUpdateUser() {
        //given
        User user = new UserBuilder().buildTestUser();
        User afterUpdate;

        //when
        repo.insertUser(user);
        user.setEmail("newEmail@xd.pl");
        boolean result = repo.updateUser(user);
        afterUpdate = repo.findByUsername(user.getUsername());

        //then
        assertTrue(result);
        assertEquals("newEmail@xd.pl", afterUpdate.getEmail());
    }

    @Test
    public void shouldAssignUserRole() {
        //given
        User user = new UserBuilder().buildTestUser();

        //when
        repo.insertUser(user);
        boolean result = repo.assignUserRole(user.getUsername());
        List<String> roles = repo.findUserRoles(user.getUsername());

        //then
        assertTrue(result);
        assertTrue(roles.size() == 1);
        assertEquals(Const.ROLE_USER, roles.get(0));
    }

    @Test
    public void shouldAssignEmployeeRole() {
        //given
        User user = new UserBuilder().buildTestUser();

        //when
        repo.insertUser(user);
        boolean result = repo.assignEmployeeRole(user.getUsername());
        List<String> roles = repo.findUserRoles(user.getUsername());

        //then
        assertTrue(result);
        assertTrue(roles.size() == 1);
        assertEquals(Const.ROLE_EMPLOYEE, roles.get(0));
    }
}
