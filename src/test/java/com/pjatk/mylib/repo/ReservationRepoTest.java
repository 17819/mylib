package com.pjatk.mylib.repo;

import com.pjatk.mylib.AbstractIntegrationTest;
import com.pjatk.mylib.model.Reservation;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ReservationRepoTest extends AbstractIntegrationTest {
    @Inject
    ReservationRepoImpl reservationRepo;

    @Test
    public void shouldFindAllReservations() {
        //given
        Integer userId = 1;

        //when
        List<Reservation> reservations = reservationRepo.getAllReservations(userId);

        //then
        assertTrue(reservations.size() == 1);
    }

    @Test
    public void shouldNotFindAnyReservations() {
        //given
        Integer userId = 9999;

        //when
        List<Reservation> reservations = reservationRepo.getAllReservations(userId);

        //then
        assertTrue(reservations.size() == 0);
    }

    @Test
    public void shouldInsertReservation() {
        //given
        Reservation reservation = new Reservation();
        reservation.setIdBook(1);
        reservation.setIdUser(1);
        reservation.setIdLibrary(1);
        reservation.setRentalDate(new Date());
        reservation.setReturnDate(new Date());

        //when
        boolean result = reservationRepo.insertReservation(reservation);

        //then
        assertTrue(result);
    }

    @Test
    public void shouldReturnFalseWhenReservationIsNull() {
        //given
        Reservation reservation = null;
        //when
        boolean result = reservationRepo.insertReservation(reservation);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldReturnFalseWhenReservationIsEmpty() {
        //given
        Reservation reservation = new Reservation();

        //when
        boolean result = reservationRepo.insertReservation(reservation);

        //then
        assertFalse(result);
    }
}
