package com.pjatk.mylib.repo;

import com.pjatk.mylib.constants.Const;
import com.pjatk.mylib.model.User;

import java.util.Date;

public class UserBuilder {
    private String name;
    private String surname;
    private String pesel;
    private String socialStatus;
    private Date birthDate;
    private String city;
    private String street;
    private String postalCode;
    private String houseNumber;
    private String apartmentNumber;
    private String username;
    private String password;
    private String email;
    private boolean enabled = true;

    public UserBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public UserBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public UserBuilder withPesel(String pesel) {
        this.pesel = pesel;
        return this;
    }

    public UserBuilder withSocialStatus(String socialStatus) {
        this.socialStatus = socialStatus;
        return this;
    }

    public UserBuilder withStreet(String street) {
        this.street = street;
        return this;
    }

    public UserBuilder withCity(String city) {
        this.city = city;
        return this;
    }

    public UserBuilder withHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
        return this;
    }

    public UserBuilder withAparmentNumber(String apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
        return this;
    }

    public UserBuilder withPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public UserBuilder withBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public UserBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public User build() {
        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setPesel(pesel);
        user.setSocialStatus(socialStatus);
        user.setBirthDate(birthDate);
        user.setCity(city);
        user.setStreet(street);
        user.setPostalCode(postalCode);
        user.setHouseNumber(houseNumber);
        user.setApartmentNumber(apartmentNumber);
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.setEnabled(enabled);
        return user;
    }

    public User buildTestUser() {
        User user = new UserBuilder()
                .withUsername("UnitTestUsername")
                .withPassword("SomePassword11")
                .withEmail("asdf@xd.pl")
                .withSocialStatus(Const.SOCIAL_STATUS_STUDENT)
                .withBirthDate(new Date())
                .withCity("Toruń")
                .withStreet("Toruńska")
                .withPesel("12345678999")
                .withPostalCode("12345")
                .withHouseNumber("15")
                .withName("Andrzej")
                .withSurname("Andrzejowski")
                .build();
        return user;
    }
}
