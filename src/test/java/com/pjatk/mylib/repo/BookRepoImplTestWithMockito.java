package com.pjatk.mylib.repo;

import com.pjatk.mylib.model.BookCopies;
import com.pjatk.mylib.model.Reservation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class BookRepoImplTestWithMockito {
    @Mock
    private EntityManager entityManager;

    @InjectMocks
    private BookRepoImpl bookRepo;

    @Test
    public void shouldThrowExceptionAndReturnFalse() {
        //given
        Integer bookId = 9999;
        Integer libraryId = 9999;

        //when
        Mockito.when(entityManager.createNamedQuery(BookCopies.DECREASE_AVAILABLE_COPIES)).thenThrow(Exception.class);
        boolean result = bookRepo.decreaseAvailableCopies(bookId, libraryId);

        //then
        assertFalse(result);
    }
}
