package com.pjatk.mylib.service;

import com.pjatk.mylib.AbstractIntegrationTest;
import com.pjatk.mylib.model.Library;
import com.pjatk.mylib.model.WorkingHours;
import org.junit.Test;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;

public class LibraryServiceTest extends AbstractIntegrationTest {
    @Inject
    LibraryService libraryService;

    @Test
    public void shouldFindAllLibraries() {
        List<Library> result = libraryService.findAllLibraries();
        assertTrue(result.size() == 2);
    }

    @Test
    public void shouldFindAllWorkingHors() {
        List<WorkingHours> result = libraryService.findAllWorkingHours();
        assertTrue(result.size() == 7);
    }
}