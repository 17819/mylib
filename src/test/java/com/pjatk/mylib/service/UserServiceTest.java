package com.pjatk.mylib.service;

import com.pjatk.mylib.AbstractIntegrationTest;
import com.pjatk.mylib.constants.Const;
import com.pjatk.mylib.model.User;
import com.pjatk.mylib.repo.UserBuilder;
import org.junit.Test;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;

public class UserServiceTest extends AbstractIntegrationTest {
    @Inject
    UserService service;

    @Test
    public void shouldReturnNewUsername() {
        //given

        //when
        String username = service.generateUsername(4);

        //then
        assertNotNull(username);
        assertEquals('U', username.charAt(0));
    }

    @Test
    public void shouldGenerateRandomPassword() {
        //given

        //when
        String password = service.generateRandomPassword(12);

        //then
        assertNotNull(password);
        assertTrue(password.length() == 12);
    }

    @Test
    public void shouldAssignUserCredentials() {
        //given
        User user = new User();

        //when
        service.assignCredentialsToUser(user);

        //then
        assertTrue(user.getUsername().length() > 0);
        assertTrue(user.getPassword().length() > 0);
    }

    @Test
    public void shouldRemoveUser() {
        //given
        String username = "admin";

        //when
        boolean result = service.removeUser(username);
        User user = service.findByUsername(username);

        //then
        assertTrue(result);
        assertNull(user);
    }

    @Test
    public void shouldAssignUserRole() {
        //given
        User user = new UserBuilder().buildTestUser();

        //when
        service.insertUser(user);
        boolean result = service.assignUserRole(user.getUsername());
        List<String> roles = service.findUserRoles(user.getUsername());

        //then
        assertTrue(result);
        assertTrue(roles.size() == 1);
        assertEquals(Const.ROLE_USER, roles.get(0));
    }

    @Test
    public void shouldAssignEmployeeRole() {
        //given
        User user = new UserBuilder().buildTestUser();

        //when
        service.insertUser(user);
        boolean result = service.assignEmployeeRole(user.getUsername());
        List<String> roles = service.findUserRoles(user.getUsername());

        //then
        assertTrue(result);
        assertTrue(roles.size() == 1);
        assertEquals(Const.ROLE_EMPLOYEE, roles.get(0));
    }

    @Test
    public void shouldInsertUser() {
        //given
        User user = new UserBuilder().buildTestUser();
        User afterInsert;

        //when
        boolean result = service.insertUser(user);
        afterInsert = service.findByUsername(user.getUsername());

        //then
        assertTrue(result);
        assertEquals("UnitTestUsername", afterInsert.getUsername());
    }

    @Test
    public void shouldUpdateUser() {
        //given
        User user = new UserBuilder().buildTestUser();
        User afterUpdate;

        //when
        service.insertUser(user);
        user.setEmail("newEmail@xd.pl");
        boolean result = service.updateUser(user);
        afterUpdate = service.findByUsername(user.getUsername());

        //then
        assertTrue(result);
        assertEquals("newEmail@xd.pl", afterUpdate.getEmail());
    }

    @Test
    public void shouldFindUserSearchingById() throws Exception {
        // given
        Integer id = 1;

        // when
        User user = service.find(id);

        // then
        assertNotNull(user);
    }

    @Test
    public void shouldNotFindAnyUserWhenSearchingById() throws Exception {
        // given
        Integer wrongId = 99999;

        // when
        User user = service.find(wrongId);

        // then
        assertNull(user);
    }

    @Test
    public void shouldNotFindAnyUserWhenSearchingByPesel() throws Exception {
        // given
        String pesel = "TOTALLY WRONG PESEL";

        // when
        User user = service.findByPesel(pesel);

        // then
        assertNull(user);
    }

    @Test
    public void shouldFindUserSearchingByPesel() throws Exception {
        // given
        String pesel = "12345678901";

        // when
        User user = service.findByPesel(pesel);

        // then
        assertNotNull(user);
        assertEquals(pesel, user.getPesel());
    }

    @Test
    public void shouldReturnTrueIfPeselExistsInDb() throws Exception {
        // given
        String pesel = "12345678901";

        // when
        boolean result = service.checkIfUserWithPeselExists(pesel);

        // then
        assertTrue(result);
    }

    @Test
    public void shouldNotReturnTrueIfPeselDoesNotExistInDb() throws Exception {
        // given
        String pesel = "WRONG PESEL";

        // when
        boolean result = service.checkIfUserWithPeselExists(pesel);

        // then
        assertFalse(result);
    }

    @Test
    public void shouldUpdateUserPassword() throws Exception {
        //given
        String username = "admin";
        String currentPassword = "admin1";
        String newPassword = "superAdmin1";

        //when
        boolean result = service.updateUserPassword(username, newPassword);
        User user = service.findByUsername(username);

        //then
        assertTrue(result);
        assertEquals(newPassword, user.getPassword());
    }

    @Test
    public void shouldUpdateUserEmail() throws Exception {
        //given
        String username = "admin";
        String currentEmail = "aaa@wp.pl";
        String newEmail = "superaaa@wp.pl";

        //when
        boolean result = service.updateUserEmail(username, newEmail);
        User user = service.findByUsername(username);

        //then
        assertTrue(result);
        assertEquals(newEmail, user.getEmail());
    }

    @Test
    public void shouldCheckIfEmailsAreCorrectAndReturnFalse() {
        assertFalse(service.checkIfEmailsAreCorrect("WRONG EMAIL", "WRONG EMAIL"));
    }

    @Test
    public void shouldCheckIfEmailsAreCorrectAndReturnTrue() {
        assertTrue(service.checkIfEmailsAreCorrect("aaa@wp.pl", "aaa@wp.pl"));
    }

    @Test
    public void shouldCheckIfPasswordsAreValidAndReturnTrue() {
        assertTrue(service.checkIfNewPasswordsAreValid("oldPassword123",
                "newPassword123", "newPassword123"));
    }

    @Test
    public void shouldCheckIfPasswordsAreValidAndReturnFalse() {
        assertFalse(service.checkIfNewPasswordsAreValid("oldPassword123",
                "oldPassword123", "oldPassword123"));
    }

    @Test
    public void shouldCheckIfOldPasswordIsValidAndReturnTrue() {
        User user = new User();
        user.setPassword("TEST");
        assertTrue(service.checkIfValidOldPassword(user, "TEST"));
    }

    @Test
    public void shouldCheckIfOldPasswordIsValidAndReturnFalse() {
        User user = new User();
        user.setPassword("TEST");
        assertFalse(service.checkIfValidOldPassword(user, "1234"));
    }
}
