package com.pjatk.mylib.service;

import com.pjatk.mylib.AbstractIntegrationTest;
import com.pjatk.mylib.model.Book;
import com.pjatk.mylib.model.BookCopies;
import com.pjatk.mylib.model.Reservation;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class BookServiceTest extends AbstractIntegrationTest {
    @Inject
    BookService service;

    @Test
    public void shouldNotFindAnyBookWhenSearchingById() throws Exception {
        // given
        Integer wrongId = 99999;

        // when
        Book book = service.find(wrongId);

        // then
        assertNull(book);
    }

    @Test
    public void shouldFindBookWhenSearchingById() throws Exception {
        // given
        Integer id = 1;

        // when
        Book book = service.find(id);

        // then
        assertNotNull(book);
    }

    @Test
    public void shouldNotFindAnyBookWhenSearchingByTitle() throws Exception {
        // given
        String title = "TOTALLY WRONG TITLE";

        // when
        List<Book> books = service.findByTitle(title);

        // then
        assertTrue(books.size() == 0);
    }

    @Test
    public void shouldFindBookWhenSearchingByTitle() throws Exception {
        // given
        String title = "Lalka";

        // when
        List<Book> books = service.findByTitle(title);

        // then
        assertTrue(books.size() == 1);
        assertEquals(title, books.get(0).getTitle());
    }

    @Test
    public void shouldNotFindAnyBookWhenSearchingByAuthor() throws Exception {
        // given
        String name = "TOTALLY WRONG NAME";
        String surname = "TOTALLY WRONG SURNAME";

        // when
        List<Book> books = service.findByAuthor(name, surname);

        // then
        assertTrue(books.size() == 0);
    }

    @Test
    public void shouldFindBookWhenSearchingByAuthor() throws Exception {
        // given
        String name = "Bolesław";
        String surname = "Prus";

        // when
        List<Book> books = service.findByAuthor(name, surname);

        // then
        assertTrue(books.size() == 2);
        assertEquals(name, books.get(0).getAuthorName());
        assertEquals(name, books.get(1).getAuthorName());
        assertEquals(surname, books.get(1).getAuthorSurname());
        assertEquals(surname, books.get(1).getAuthorSurname());
    }

    @Test
    public void shouldNotFindAnyBookWhenSearchingByIsbn() throws Exception {
        // given
        String isbn = "TOTALLY WRONG ISBN";

        // when
        List<Book> books = service.findByIsbn(isbn);

        // then
        assertTrue(books.size() == 0);
    }

    @Test
    public void shouldFindBookWhenSearchingByIsbn() throws Exception {
        // given
        String isbn = "9788377792063";

        // when
        List<Book> books = service.findByIsbn(isbn);

        // then
        assertTrue(books.size() == 1);
        assertEquals(isbn, books.get(0).getIsbn());
    }

    @Test
    public void shouldNotFindAnyBookWhenSearchingByPublisher() throws Exception {
        // given
        String publisher = "TOTALLY WRONG PUBLISHER";

        // when
        List<Book> books = service.findByPublisher(publisher);

        // then
        assertTrue(books.size() == 0);
    }

    @Test
    public void shouldFindBookWhenSearchingByPublisher() throws Exception {
        // given
        String publisher = "Wydawnictwo MG";

        // when
        List<Book> books = service.findByPublisher(publisher);

        // then
        assertTrue(books.size() == 1);
        assertEquals(publisher, books.get(0).getPublisher());
    }

    @Test
    public void shouldNotFindAnyBookWhenSearchingBySignature() throws Exception {
        // given
        String signature = "TOTALLY WRONG SIGNATURE";

        // when
        List<Book> books = service.findByPublisher(signature);

        // then
        assertTrue(books.size() == 0);
    }

    @Test
    public void shouldFindBookWhenSearchingBySignature() throws Exception {
        // given
        String signature = "LS0001";

        // when
        List<Book> books = service.findBySignature(signature);

        // then
        assertTrue(books.size() == 3);
        assertEquals(signature, books.get(0).getSignature());
        assertEquals(signature, books.get(1).getSignature());
        assertEquals(signature, books.get(2).getSignature());
    }

    @Test
    public void shouldReturnAmountOfAvailableBooks() throws Exception {
        //given
        Integer bookId = 1;

        //when
        List<BookCopies> amountOfFreeCopies = service.findAmountOfFreeCopies(bookId);

        //then
        assertNotNull(amountOfFreeCopies);
        assertTrue(amountOfFreeCopies.size() == 2);
        assertTrue(amountOfFreeCopies.get(0).getFree() == 3);
    }

    @Test
    public void shouldNotReturnAmountOfAvailableBooks() throws Exception {
        //given
        Integer wrongId = 9999;

        //when
        List<BookCopies> amountOfFreeCopies = service.findAmountOfFreeCopies(wrongId);

        //then
        assertTrue(amountOfFreeCopies.size()==0);
    }

    @Test
    public void shouldFindAllReservations() {
        //given
        Integer userId = 1;

        //when
        List<Reservation> reservations = service.getAllReservations(userId);

        //then
        assertTrue(reservations.size() == 1);
    }

    @Test
    public void shouldNotFindAnyReservations() {
        //given
        Integer userId = 9999;

        //when
        List<Reservation> reservations = service.getAllReservations(userId);

        //then
        assertTrue(reservations.size() == 0);
    }

    @Test
    public void shouldInsertReservation() {
        //given
        Reservation reservation = new Reservation();
        reservation.setIdBook(1);
        reservation.setIdUser(1);
        reservation.setIdLibrary(1);
        reservation.setRentalDate(new Date());
        reservation.setReturnDate(new Date());

        //when
        boolean result = service.insertReservation(reservation);

        //then
        assertTrue(result);
    }

    @Test
    public void shouldReturnFalseWhenReservationIsNull() {
        //given
        Reservation reservation = null;
        //when
        boolean result = service.insertReservation(reservation);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldReturnFalseWhenReservationIsEmpty() {
        //given
        Reservation reservation = new Reservation();

        //when
        boolean result = service.insertReservation(reservation);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldDecreaseAvailableCopies() throws Exception {
        //given
        Integer bookId = 1;
        Integer liraryId = 1;

        //when
        boolean result = service.decreaseAvailableCopies(bookId, liraryId);

        //then
        assertTrue(result);
        assertEquals(service.findAmountOfFreeCopies(bookId).get(0).getFree(), new Integer(2));
    }
}
