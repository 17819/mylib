package com.pjatk.mylib.config;

import com.pjatk.mylib.model.Reservation;
import com.pjatk.mylib.repo.*;
import com.pjatk.mylib.service.BookService;
import com.pjatk.mylib.service.LibraryService;
import com.pjatk.mylib.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
public class TestConfig {
    private String driverClassName = "com.mysql.cj.jdbc.Driver";
    private String url = "jdbc:mysql://localhost:3306/unittest?serverTimezone=CET";
    private String username = "root";
    private String password = "root";

    @Bean
    public BookRepoImpl bookRepo() {
        return new BookRepoImpl();
    }

    @Bean
    public UserRepoImpl userRepo() {
        return new UserRepoImpl();
    }

    @Bean
    public ReservationRepoImpl reservationRepo() {
        return new ReservationRepoImpl();
    }

    @Bean
    public BookService bookService() {
        return new BookService();
    }

    @Bean
    public UserService userService() {
        return new UserService();
    }

    @Bean
    public LibraryRepoImpl libraryRepo() {
        return new LibraryRepoImpl();
    }

    @Bean
    public LibraryService libraryService() {
        return new LibraryService();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emFactory = new LocalContainerEntityManagerFactoryBean();
        emFactory.setPersistenceUnitName("library");
        emFactory.setDataSource(dataSource());
        emFactory.setPackagesToScan(new String[] { "com.pjatk.mylib.model" });
        emFactory.setJpaVendorAdapter(createHibernateAdapter());
        return emFactory;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    private HibernateJpaVendorAdapter createHibernateAdapter() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setGenerateDdl(true);
        hibernateJpaVendorAdapter.setDatabase(Database.MYSQL);
        return hibernateJpaVendorAdapter;
    }

    @Bean
    @Autowired
    public JpaTransactionManager transactionManager(EntityManagerFactory em) {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(em);
        return txManager;
    }
}
