package com.pjatk.mylib.validation;

import com.pjatk.mylib.constants.Const;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserValidatorTest {
    @Test
    public void test_checkIfNewPasswordsAreValid_returns_false_when_input_is_null() {
        perform_checkIfNewPasswordsAreValidTest(null, null, null, 6, 25, false);
    }

    @Test
    public void test_checkIfNewPasswordsAreValid_returns_false_when_all_passwords_are_same() {
        perform_checkIfNewPasswordsAreValidTest("theSamePassword", "theSamePassword", "theSamePassword",
                6, 25, false);
    }

    @Test
    public void test_checkIfNewPasswordsAreValid_returns_true_when_all_passwords_are_correct() {
        perform_checkIfNewPasswordsAreValidTest("theSamePassword", "NewPassword", "NewPassword",
                6, 25, true);
    }

    @Test
    public void test_checkIfNewPasswordsAreValid_returns_false_when_passwords_are_too_long() {
        perform_checkIfNewPasswordsAreValidTest("oldPassword", "newTooLongPassword", "newTooLongPassword",
                6, 10, false);
    }

    @Test
    public void test_checkIfNewPasswordsAreValid_returns_false_when_passwords_are_too_short() {
        perform_checkIfNewPasswordsAreValidTest("oldPassword", "tooShort", "tooShort",
                10, 25, false);
    }

    private void perform_checkIfNewPasswordsAreValidTest(String old, String new1, String new2,
                                                            Integer min, Integer max, boolean expectedResult) {
        boolean result = UserValidator.checkIfNewPasswordsAreValid(old, new1, new2, min, max);
        assertEquals(result, expectedResult);
    }

    @Test
    public void test_checkIfEmailsAreCorrect_returns_false_when_input_is_null() {
        assertFalse(UserValidator.checkIfEmailsAreCorrect(null, null));
    }

    @Test
    public void test_checkIfEmailsAreCorrect_returns_false_when_emails_does_not_match() {
        assertFalse(UserValidator.checkIfEmailsAreCorrect("onemail@wp.pl", "twomail@wp.pl"));
    }

    @Test
    public void test_checkIfEmailsAreCorrect_returns_false_when_emails_does_not_match_pattern() {
        assertFalse(UserValidator.checkIfEmailsAreCorrect("WRONG_EMAIL", "WRONG_EMAIL"));
    }

    @Test
    public void test_checkIfEmailsAreCorrect_returns_true_when_emails_are_correct() {
        assertTrue(UserValidator.checkIfEmailsAreCorrect("aaa@wp.pl", "aaa@wp.pl"));
    }

    @Test
    public void test_checkIfUserIsAdminOrEmployee_returns_false_when_input_is_null() {
        assertFalse(UserValidator.checkIfUserIsAdminOrEmployee(null));
    }

    @Test
    public void test_checkIfUserIsAdminOrEmployee_returns_true_when_there_is_admin_role() {
        List<String> roles = new ArrayList<>();
        roles.add(Const.ROLE_ADMIN);
        roles.add(Const.ROLE_USER);
        assertTrue(UserValidator.checkIfUserIsAdminOrEmployee(roles));
    }

    @Test
    public void test_checkIfUserIsAdminOrEmployee_returns_true_when_there_is_employee_role() {
        List<String> roles = new ArrayList<>();
        roles.add(Const.ROLE_EMPLOYEE);
        roles.add(Const.ROLE_USER);
        assertTrue(UserValidator.checkIfUserIsAdminOrEmployee(roles));
    }

    @Test
    public void test_checkIfUserIsAdminOrEmployee_returns_false_when_there_is_only_user_role() {
        List<String> roles = new ArrayList<>();
        roles.add(Const.ROLE_USER);
        assertFalse(UserValidator.checkIfUserIsAdminOrEmployee(roles));
    }

    @Test
    public void test_checkIfUserIsAdmin_returns_false_when_input_is_null() {
        assertFalse(UserValidator.checkIfUserIsAdmin(null));
    }

    @Test
    public void test_checkIfUserIsAdmin_returns_true_when_there_is_admin_role() {
        List<String> roles = new ArrayList<>();
        roles.add(Const.ROLE_ADMIN);
        roles.add(Const.ROLE_USER);
        assertTrue(UserValidator.checkIfUserIsAdmin(roles));
    }

    @Test
    public void test_checkIfUserIsAdmin_returns_false_when_there_is_no_admin_role() {
        List<String> roles = new ArrayList<>();
        roles.add(Const.ROLE_EMPLOYEE);
        roles.add(Const.ROLE_USER);
        assertFalse(UserValidator.checkIfUserIsAdmin(roles));
    }

    @Test
    public void test_checkIfUserIsEmployee_returns_false_when_input_is_null() {
        assertFalse(UserValidator.checkIfUserIsEmployee(null));
    }

    @Test
    public void test_checkIfUserIsEmployee_returns_true_when_there_is_employee_role() {
        List<String> roles = new ArrayList<>();
        roles.add(Const.ROLE_EMPLOYEE);
        roles.add(Const.ROLE_USER);
        assertTrue(UserValidator.checkIfUserIsEmployee(roles));
    }

    @Test
    public void test_checkIfUserIsEmployee_returns_false_when_there_is_no_employee_role() {
        List<String> roles = new ArrayList<>();
        roles.add(Const.ROLE_ADMIN);
        roles.add(Const.ROLE_USER);
        assertFalse(UserValidator.checkIfUserIsEmployee(roles));
    }
}